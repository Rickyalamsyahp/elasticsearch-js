'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _basic = require('./basic.conf');

var _basic2 = _interopRequireDefault(_basic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mode = process.env.NODE_ENV;
var tokenNames = {
  pajakku: {
    accessToken: 'MPK_ACCESS_TOKEN',
    refreshToken: 'MPK_REFRESH_TOKEN'
  },
  sobatpajak: {
    accessToken: 'SP_ACCESS_TOKEN',
    refreshToken: 'SP_REFRESH_TOKEN'
  }
};

var env = require('./' + mode + '.' + _basic2.default.theme + '.js' || {}).default;

var all = _lodash2.default.merge(_basic2.default, env, { tokenNames: tokenNames[_basic2.default.theme] });

exports.default = all;
//# sourceMappingURL=index.js.map