'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _constant = require('../constant');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  env: process.env.NODE_ENV,
  root: _path2.default.normalize(__dirname + '/../..'),
  corsHeaders: ['x-pagination-count'],
  enabledAuth: true,

  cookieExpiresMultiplier: 900,
  cookie: {
    path: '/',
    secure: false,
    httpOnly: false
  },

  cron: {
    privateToken: '*/1 * * * *'
  },

  locale: {
    code: 'id',
    options: [{ label: 'Bahasa', value: 'id' }, { label: 'English', value: 'en' }]
  },

  theme: _constant.themes.PAJAKKU,
  applicationType: process.env.APPLICATION_TYPE || _constant.applicationTypes.INTERNAL
};
//# sourceMappingURL=basic.conf.js.map