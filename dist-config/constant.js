'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var themes = exports.themes = {
  PAJAKKU: 'pajakku',
  SOBATPAJAK: 'sobatpajak'
};

var applicationTypes = exports.applicationTypes = {
  INTERNAL: 'internal',
  PRODUCT: 'product',
  GENERAL: 'general'
};

var efiling = exports.efiling = {
  PATH: "/djp/get_ntte/{filename}/{ntpa}"
};

var Bkwp = exports.Bkwp = {
  PATH: "/bkwp/konfirmasi/npwp"
};
//# sourceMappingURL=constant.js.map