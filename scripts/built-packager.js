var fse = require('fs-extra')
var rimraf = require('rimraf');
var path = require('path');

rimraf('./built', function(){
  fse.ensureDir('./build')
  fse.ensureDir('./dist')
  fse.copy('package.json', path.join('built', 'package.json'));
  fse.copy('./build', path.join('built', 'public'));
  fse.copy('./dist', path.join('built', 'server'));
  fse.copy('./dist-config', path.join('built', 'config'));
})

console.log('built completed')