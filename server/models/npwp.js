const mongoose = require("mongoose");

const npwpSchema = new mongoose.Schema({
  npwp: {
    type: String,
    required: true,
  },
  username:{
    type: String,
    required: true,
  },
  nama: {
    type: String,
    required: true,
  },
  alias: {
    type: String,
  }
});

module.exports = mongoose.model("npwp", npwpSchema);
