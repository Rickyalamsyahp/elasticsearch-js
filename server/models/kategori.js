const mongoose = require("mongoose");

const kategoriSchema = new mongoose.Schema({
  kategori: {
    type: String,
    required: true,
  },
  kop: {
    type: String,
    required: true,
  },
  namaMenu: {
    type: String,
    required: true,
  },
  kodemap: {
    type: String,
    required: true,
  },
  fg: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model("kategori", kategoriSchema);
