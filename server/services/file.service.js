const FormData = require('form-data');

module.exports = {
  fileToFormData: (file, meta = {}, fileLabel='file') => {
    const data = new FormData();
    data.append(fileLabel, file);
    const keys = Object.keys(meta);
    for(let i = 0 ; i < keys.length ; i++){
      const key = keys[i];
      data.append(key, meta[key]);
    }
    return data;
  }
}