module.exports = {
  parseNPWP:(npwp) => {
    let result = String(npwp).split('');
    result.splice(2, 0, '.');
    result.splice(6, 0, '.');
    result.splice(10, 0, '.');
    result.splice(12, 0, '-');
    result.splice(16, 0, '.');
    return result.toString().replace(/,/g,'');
  }
}