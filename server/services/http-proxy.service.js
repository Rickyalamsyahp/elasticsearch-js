import axios from 'axios'
import request from 'request'
import config from '../../config/environment'
import { generatePajakkuProfile } from './auth.service'
import fs from 'fs'
import { apiError } from '.'
import qs from 'query-string'

export const index = (url, req, res, options={}) => {
  return new Promise(async (resolve, reject) => {
    try {
      req.headers['content-type'] = 'application/json';
      req.headers['x-no-compression'] = true;
      req.headers['Host'] = config.private.hostName;

      req.headers['x-client'] = config.private.credentials.client_id;
      if(options.overideProfile){
        req.headers['x-profile'] = await generatePajakkuProfile(req.user)
      } else {
        delete req.headers['x-profile']
      }
      
      if(req.query && Object.keys(req.query).length > 0){
        url += `?${qs.stringify(req.query)}`;
      }

      const method = (options.method || req.method).toLowerCase();
      let result = method === 'get' || method === 'delete' ? 
        await axios[method](url,{headers:req.headers}) : 
        await axios[method](url,options.data || req.body,{headers:req.headers});

      // if(result.headers) res.set(result.headers);
      resolve(result)
    } catch (err){
      // errorHandler(err, res)
      console.log(err)
      reject(err)
    }
  })
} 

export const upload = (url, req, res, options={}) => {
  let opt = Object.assign({
    fileKey: 'file'
  }, options)
  return new Promise(async (resolve, reject) => {
    try{
      let formData = {
        originalname: req.file.originalname,
        mimetype: req.file.mimetype,
        size: req.file.size
      }

      for(let key of Object.keys(req.body)){
        formData[key] = req.body[key]
      }

      formData[opt.fileKey] = fs.createReadStream(req.file.path)

      let headers = {
        authorization:req.headers.authorization,
        'x-client': config.private.credentials.client_id,
      }

      if(options.overideProfile){
        headers['x-profile'] = await generatePajakkuProfile(req.user)
      }
    
      const opts = {
        uri: url,
        method:'POST',
        headers,
        formData
      }

      request(opts, (err, response) => {
        if(err) {
          reject(new apiError(err, 400))
        }else {
          if(response.statusCode === 200){
            resolve(response)
          } else {
            reject(new apiError(response.body, response.statusCode))
          }
        }
      });
      
    } catch (err){
      reject(err)
    }
  })
}

export const errorHandler = (res, err) => {
  if(err.response.status === 401) cache.clear();
  const keys = Object.keys(err.response.headers);
  for(let key of keys){
    if(['server', 'content-type', 'date', 'transfer-encoding', 'connection', 'vary'].indexOf(key) < 0)
      res.set(key, err.response.headers[key]);
    // res.set(err.response.headers);
  }
  return res.status(err.response.status).json(err.response.data ? err.response.data : {errorMessage: 'Something Wrong'})
}