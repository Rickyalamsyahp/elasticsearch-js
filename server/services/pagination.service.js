export const mongo = async (req, res, model, selector={}, findOption='', aggregator) => {
  let { page=1, limit=20, keyword, column, order, orderBy, ne } = req.query;

  if(column && keyword) {
      let regex = '^';
      var _regex = keyword.split(' ');
      for(var i in _regex){
          regex += '(?=.*'+_regex[i]+')';
      }
      regex += '.+';
      selector[column] = {$regex: new RegExp(regex,'ig')};
  }

  let sort = {}
  if(order && orderBy) {
      sort[orderBy] = order === 'asc' ? 1 : -1;
  } else {
    sort = {dateCreated:-1}
  }

  if(ne){
      ne = ne.split(';')
      selector[ne[0]] = {$ne: ne[1]}
  }

  let result;
  let count = await model.count(selector)
  if(aggregator){
      let opts = aggregator.concat([
        {$match:selector},
        {$skip:Number(limit)*(Number(page)-1)},
        {$limit:Number(limit)},
        {$sort:sort},
      ])
      result = await model.aggregate(opts)
  } else {
    result = await model.find(selector, findOption)
        .skip(Number(limit)*(Number(page)-1))
        .limit(Number(limit))
        .sort(sort);
  }

  res.set('x-pagination-count',count);

  return result
}

export const query =  (req, res, sql, columnMapper, selector) => {
  let { page=1, size=20, sort='asc', sortBy='id', column, keyword } = req.query;
  let splitColumn;
  let selectorType = typeof selector;
  let order = sort;
  let orderBy = sortBy;
  if(columnMapper){
    orderBy = columnMapper(orderBy);
    splitColumn = column ? columnMapper(column).split('.') : '';
  }

  if(column && keyword){
    // sql.whereRaw(`"${column}" LIKE ?`, `%${keyword.toLowerCase()}%`)
    sql.whereRaw(`LOWER(${splitColumn ? `"${splitColumn[0]}"."${splitColumn[1]}"` : `"${column}"`}) LIKE ?`, `%${keyword.toLowerCase()}%`)
  }

  if(selector) {
    if(selectorType === 'function') selector(sql);
    else sql.where(selector);
  }
  
  if(page){
    sql.limit(size);
    sql.offset((page-1)*size)
  }

  if(order){
    sql.orderBy(orderBy, String(order).toLowerCase())
  }

  
  return sql
}

export const total = (req, res, db, selector, columnMapper=col => (col)) => {
  return new Promise(async resolve => {
    let { page, column, keyword } = req.query; 
    column = columnMapper(column);
    let splitColumn;

    let selectorType = typeof selector;
    if(page){
      let _db = db.count('* as count');
      if(column && keyword) _db.whereRaw(`LOWER(${splitColumn ? `"${splitColumn[0]}"."${splitColumn[1]}"` : `"${column}"`}) LIKE ?`, `%${keyword.toLowerCase()}%`);
      if(selector) {
        if(selectorType === 'function') selector(_db);
        else _db.where(selector);
      }
      let total = await _db.first();
      // let total = await (column && keyword ? _db.whereRaw(`LOWER(${column}) LIKE ?`, `%${keyword.toLowerCase()}%`).first() : _db.first());
      res.set('x-pagination-count', total.count);
      resolve(total.count);
    } else resolve();
  })
}