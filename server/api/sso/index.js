import express from 'express';
import * as controller from './sso.controller';

let router = express.Router();

router.get('/login', controller.login);
router.get('/exchangeToken/:code', controller.exchangeToken);
router.get('/refreshToken/:refreshToken', controller.refreshToken);
router.get('/me', controller.me);
router.get('/company/:companyId/me', controller.me);

export default router;