import { wrapAsync } from "../../services";
import _ , {merge}  from "lodash"
import config from "../../../config/environment";
var mongoose = require("mongoose");
const Kategori = require("../../models/kategori");
const Npwp = require("../../models/npwp");


const mongoDB = config.mongodb.hosts+config.mongodb.database;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));


export const mongodb = {
  kategori: async (req, res) => {
    {
      try {
        const data = await Kategori.find();
        res.send(data);
      } catch (error) {
        res.status(500).json({ message: "Internal server error" });
      }
    }
  },
  getForMenukategori: async (req, res) => {
    {
      try {
        const data = await Kategori.find({namaMenu: req.query.namaMenu});
        // const data_ =[]
        // for (let a = 0; a < data.length; a++) {
        //   const element = data[a];
        //   data_.push({
        //     kategori: element.kategori,
        //   })  
        // }
        // const dataUniqBy= _.uniqBy(data_, "kategori")
        res.send(data)
      } catch (error) {
        res.status(500).json({ message: "Internal server error" });
      }
    }
  },
  getForMenuMap: async (req, res) => {
    {
      try {
        const data = await Kategori.find({namaMenu: req.query.namaMenu});
        const data_ =[]
        for (let a = 0; a < data.length; a++) {
          const element = data[a];
          data_.push({
            kodemap: element.kodemap,
          })  
        }
        const dataUniqBy= _.uniqBy(data_, "kodemap")
        res.send(dataUniqBy)
      } catch (error) {
        res.status(500).json({ message: "Internal server error" });
      }
    }
  },
  alias: async (req, res) => {
      try {
        const data = await Npwp.find({username: req.user.username});
        res.send(data);
      } catch (error) {
        res.status(500).json({ message: "Internal server error" });
      }
    
  },
  aliasByNpwp: async (req, res) => {
    
      try {
        const data = await Npwp.findOne({npwp: req.params.npwp});
        res.send(data);
      } catch (error) {
        res.status(500).json({ message: "Internal server error" });
      }
    
  },
};

exports.createData = wrapAsync(async(req, res) => {
  const dataclp = await Kategori.create(req.body);
  return dataclp
});

exports.createDataNpwp = wrapAsync(async(req, res) => {
  req.body.username = req.user.username;
  const dataclp = await Npwp.updateOne({
    npwp: req.body.npwp,
    nama: req.body.nama,
  }, req.body, {upsert: true})
  return dataclp
});

exports.deleteKategori = (req, res) => {
  let dataClpId = req.params.id;

  Kategori.findByIdAndRemove(dataClpId)
    .select("-__v -_id")
    .then((kategori) => {
      if (!kategori) {
        res.status(404).json({
          message: "Does Not exist a client path with id = " + dataClpId,
          error: "404",
        });
      }
      res.status(200).json({});
    })
    .catch((err) => {
      return res.status(500).send({
        message: "Error -> Can NOT delete a client path with id = " + dataClpId,
        error: err.message,
      });
    });
};

exports.update = wrapAsync(async(req, res) => {
  let item = await Kategori.findById(req.params.id);
  delete req.body._id
  let update = merge(item, req.body)
  await update.save()
  return update
});
