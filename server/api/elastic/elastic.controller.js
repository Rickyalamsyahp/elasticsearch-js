"use strict";

import config from "../../../config/environment";
const es = require("elasticsearch");
const esClient = new es.Client(config.elastic);
const Kategori = require("../../models/kategori");

// Search data option
let getDataPph = async (req) => {
  let query = {
    index: "businessintelligence*",
    type: "_doc",
    size: 100,
    body: {
      query: {
        bool: {
          must: {
            match_phrase: {
              namaMenu: req.query.namaMenu,
            },
          },
        },
      },
      aggs: {
        jenisPajak: {
          terms: {
            field: "jenisPajak.keyword",
          },
          aggs: {
            npwp: {
              terms: {
                field: "npwp.keyword",
              },
              aggs: {
                kategori: {
                  terms: {
                    field: "kategori.keyword",
                  },
                  aggs: {
                    kodemap: {
                      terms: {
                        field: "kodeMap.keyword",
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  };
  return esClient.search(query);
};

let getDataPPN = async (req) => {
  let query = {
    index: "businessintelligence*",
    type: "_doc",
    size: 100,
    body: {
      query: {
        bool: {
          must: {
            match_phrase: {
              namaMenu: req.query.namaMenu,
            },
          },
        },
      },
      aggs: {
        jenisPajak: {
          terms: {
            field: "jenisPajak.keyword",
          },
        },
      },
    },
  };
  return esClient.search(query);
};

let getDataPph42 = async (req) => {
  let query = {
    index: "businessintelligence*",
    type: "_doc",
    size: 100,
    body: {
      query: {
        bool: {
          must: {
            match_phrase: {
              namaMenu: "PPH",
            },
          },
        },
      },
      aggs: {
        jenisPajak: {
          terms: {
            field: "jenisPajak.keyword",
          },
          aggs: {
            npwp: {
              terms: {
                field: "npwp.keyword",
              },
              aggs: {
                kategori: {
                  terms: {
                    field: "kategori.keyword",
                  },
                  aggs: {
                    kodemap: {
                      terms: {
                        field: "kodeMap.keyword",
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  };
  return esClient.search(query);
};

let getDataBKWP = async (req) => {
  let query = {
    index: "businessintelligence*",
    track_total_hits: true,
    type: "_doc",
    body: {
      query: {
        bool: {
          must: {
            match_phrase: {
              namaMenu: req.query.namaMenu,
            },
          },
        },
      },
      aggs: {
        jenisPajak: {
          terms: {
            field: "jenisPajak.keyword",
          },
          aggs: {
            npwpCompany: {
              terms: {
                field: "npwpCompany.keyword",
              },
              aggs: {
                status: {
                  terms: {
                    field: "status.keyword",
                  },
                },
              },
            },
          },
        },
      },
    },
  };
  return esClient.search(query);
};

// Search data chart tahun awal
let getDataChartPPH4 = async (req) => {
  if (
    req.query.jenisPajak === "Jumlah Setor Sendiri" ||
    req.query.jenisPajak === "Jumlah Bukti Potong"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      pembetulan: {
                        terms: {
                          field: "pembetulan",
                          size: 1,
                          order: { _key: "desc" },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak === "Jumlah Setor Sendiri"
          ? { namaMenu: req.query.namaMenu, fg: 1 }
          : { namaMenu: req.query.namaMenu, fg: 0 }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let objRentangWaktu = {};
      rentangWaktu = "periode";
      objRentangWaktu[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: objRentangWaktu,
      });
    }

    return esClient.search(query);
  }
  if (
    req.query.jenisPajak ===
      "Perbandingan PPh terutang Per Kategori (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang (Rupiah)" ||
    req.query.jenisPajak === "Perbandingan KB/LB (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak ===
          "Perbandingan PPh terutang Per Kategori (Rupiah)"
          ? { kategori: req.query.kategori }
          : req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
          ? { kodemap: req.query.kodeMap }
          : { namaMenu: req.query.namaMenu }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                  // size:10,
                  // min_doc_count: 0
                },
                aggs: {
                  tanggal: {
                    terms: {
                      field: "tanggalLapor",
                      format: "dd",
                      // min_doc_count: 0
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPH",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte: rentangWaktu === "periode" ? `${tahun}-${awalbulan}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataChartPPH4TahunBanding = async (req) => {
  if (
    req.query.jenisPajak === "Jumlah Setor Sendiri" ||
    req.query.jenisPajak === "Jumlah Bukti Potong"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}`,
                  },
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      pembetulan: {
                        terms: {
                          field: "pembetulan",
                          size: 1,
                          order: { _key: "desc" },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak === "Jumlah Setor Sendiri"
          ? { namaMenu: req.query.namaMenu, fg: 1 }
          : { namaMenu: req.query.namaMenu, fg: 0 }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let objRentangWaktu = {};
      rentangWaktu = "periode";
      objRentangWaktu[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: objRentangWaktu,
      });
    }

    return esClient.search(query);
  }
  if (
    req.query.jenisPajak ===
      "Perbandingan PPh terutang Per Kategori (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang (Rupiah)" ||
    req.query.jenisPajak === "Perbandingan KB/LB (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak ===
          "Perbandingan PPh terutang Per Kategori (Rupiah)"
          ? { kategori: req.query.kategori }
          : req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
          ? { kodemap: req.query.kodeMap }
          : { namaMenu: req.query.namaMenu }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                  // size:10,
                  // min_doc_count: 0
                },
                aggs: {
                  tanggal: {
                    terms: {
                      field: "tanggalLapor",
                      format: "dd",
                      // min_doc_count: 0
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPH",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataChartPPN = async (req) => {
  if (req.query.jenisPajak === "Jumlah PPN Terutang") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.kodeMap ===
                                    "PPN Dalam Negeri Setoran Masa"
                                      ? "lampiranInduk.jmlIIDPPN"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Kegiatan Membangun Sendiri"
                                      ? "lampiranInduk.jmlIIIB"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Pembayaran Kembali Pajak Masukan Bagi Pkp Gagal Berproduksi"
                                      ? "lampiranInduk.jmlIVA"
                                      : "lampiranInduk.jmlVCPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Perbandingan KB/LB") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "lampiranInduk.jmlIIDPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              sum_b1: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB1PPN",
                                },
                              },
                              sum_b2: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB2PPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === "Jumlah PPN terkait impor terutang" ||
    req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.jenisPajak ===
                                    "Jumlah PPN terkait impor terutang"
                                      ? "lampiranB1.jmlIIAPPN"
                                      : "lampiranA1.jmlIADPP",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || 12}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun || req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  tanggal: {
                    terms: {
                      field: "tanggalLapor",
                      format: "dd",
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte: rentangWaktu === "periode" ? `${tahun}-${awalbulan}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Bayar") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}-01`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding || req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding || req.query.tahun}-12`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  tanggal: {
                    terms: {
                      field: "tanggalBayar",
                      format: "dd",
                    },
                    aggs: {
                      max_date: {
                        max: {
                          field: "tanggalBayar",
                          format: "dd",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte: rentangWaktu === "periode" ? `${tahun}-${awalbulan}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataChartPPNTahunBanding = async (req) => {
  if (req.query.jenisPajak === "Jumlah PPN Terutang") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.kodeMap ===
                                    "PPN Dalam Negeri Setoran Masa"
                                      ? "lampiranInduk.jmlIIDPPN"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Kegiatan Membangun Sendiri"
                                      ? "lampiranInduk.jmlIIIB"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Pembayaran Kembali Pajak Masukan Bagi Pkp Gagal Berproduksi"
                                      ? "lampiranInduk.jmlIVA"
                                      : "lampiranInduk.jmlVCPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Perbandingan KB/LB") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "lampiranInduk.jmlIIDPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              sum_b1: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB1PPN",
                                },
                              },
                              sum_b2: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB2PPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === "Jumlah PPN terkait impor terutang" ||
    req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.jenisPajak ===
                                    "Jumlah PPN terkait impor terutang"
                                      ? "lampiranB1.jmlIIAPPN"
                                      : "lampiranA1.jmlIADPP",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || 12}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  tanggal: {
                    terms: {
                      field: "tanggalLapor",
                      format: "dd",
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahunbanding}-${awalbulan}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Bayar") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}-01`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  tanggal: {
                    terms: {
                      field: "tanggalBayar",
                      format: "dd",
                    },
                    aggs: {
                      max_date: {
                        max: {
                          field: "tanggalBayar",
                          format: "dd",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataChartEBilling = async (req) => {
  let query = {
    index: "businessintelligence*",
    type: "_doc",
    body: {
      sort: { periode: "asc" },
      query: {
        bool: {
          should: [],
          minimum_should_match: 1,
          filter: [],
        },
      },
      aggs: {
        total_count: {
          date_histogram: {
            field: "periode",
            calendar_interval: req.query.rentangWaktu === "tahun" ? "1y" : "1M",
            format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
            min_doc_count: 0,
            extended_bounds: {
              min:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahun}-${req.query.awalbulan}`
                  : `${req.query.tahun}`,
              max:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahun || req.query.tahun}-${
                      req.query.akhirbulan || req.query.awalbulan
                    }`
                  : `${req.query.tahun}`,
            },
          },
          aggs: {
            jenisPajak: {
              terms: {
                field: "jenisPajak.keyword",
              },
              aggs: {
                npwp: {
                  terms: {
                    field: "npwp.keyword",
                  },
                  aggs: {
                    jumlah: {
                      sum: {
                        field: "jumlahTransaksi",
                      },
                    },
                    jumlahSsp: {
                      sum: {
                        field: "jumlahSsp",
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  };
  if (req.query.npwp && req.query.jenisPajak) {
    for (let item of req.query.npwp.split(",")) {
      let obj = {
        bool: {
          must: [
            {
              match_phrase: {
                namaMenu: req.query.namaMenu,
              },
            },
            {
              match_phrase: {
                npwp: item,
              },
            },
            {
              match_phrase: {
                jenisPajak: req.query.jenisPajak,
              },
            },
          ],
        },
      };
      query.body.query.bool.should.push(obj);
    }
  }
  if (req.query.tahun) {
    let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
      req.query;
    let obj = {};
    rentangWaktu = "periode";
    obj[rentangWaktu] = {
      gte: rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
      lte:
        rentangWaktu === "periode"
          ? `${tahun}-${akhirbulan || awalbulan || "12"}`
          : tahun,
    };
    query.body.query.bool.filter.push({
      range: obj,
    });
  }
  return esClient.search(query);
};

let getDataChartEBillingTahunBanding = async (req) => {
  let query = {
    index: "businessintelligence*",
    type: "_doc",
    body: {
      sort: { periode: "asc" },
      query: {
        bool: {
          should: [],
          minimum_should_match: 1,
          filter: [],
        },
      },
      aggs: {
        total_count: {
          date_histogram: {
            field: "periode",
            calendar_interval: req.query.rentangWaktu === "tahun" ? "1y" : "1M",
            format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
            min_doc_count: 0,
            extended_bounds: {
              min:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                  : `${req.query.tahunbanding}`,
              max:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahunbanding || req.query.tahun}-${
                      req.query.akhirbulan || req.query.awalbulan
                    }`
                  : `${req.query.tahunbanding}`,
            },
          },
          aggs: {
            jenisPajak: {
              terms: {
                field: "jenisPajak.keyword",
              },
              aggs: {
                npwp: {
                  terms: {
                    field: "npwp.keyword",
                  },
                  aggs: {
                    jumlah: {
                      sum: {
                        field: "jumlahTransaksi",
                      },
                    },
                    jumlahSsp: {
                      sum: {
                        field: "jumlahSsp",
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  };
  if (req.query.npwp && req.query.jenisPajak) {
    for (let item of req.query.npwp.split(",")) {
      let obj = {
        bool: {
          must: [
            {
              match_phrase: {
                namaMenu: req.query.namaMenu,
              },
            },
            {
              match_phrase: {
                npwp: item,
              },
            },
            {
              match_phrase: {
                jenisPajak: req.query.jenisPajak,
              },
            },
          ],
        },
      };
      query.body.query.bool.should.push(obj);
    }
  }
  if (req.query.tahunbanding) {
    let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
      req.query;
    let obj = {};
    rentangWaktu = "periode";
    obj[rentangWaktu] = {
      gte:
        rentangWaktu === "periode"
          ? `${tahunbanding}-${awalbulan || "01"}`
          : tahun,
      lte:
        rentangWaktu === "periode"
          ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
          : tahunbanding,
    };
    query.body.query.bool.filter.push({
      range: obj,
    });
  }
  return esClient.search(query);
};

let getDataChartBKWP = async (req) => {
  let query = {
    index: "businessintelligence*",
    type: "_doc",
    body: {
      sort: { periode: "asc" },
      query: {
        bool: {
          should: [],
          minimum_should_match: 1,
          filter: [],
        },
      },
      aggs: {
        total_count: {
          date_histogram: {
            field: "periode",
            calendar_interval: req.query.rentangWaktu === "tahun" ? "1y" : "1M",
            format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
            min_doc_count: 0,
            extended_bounds: {
              min:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahun}-${req.query.awalbulan}`
                  : `${req.query.tahun}`,
              max:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahun}-${
                      req.query.akhirbulan || req.query.awalbulan
                    }`
                  : `${req.query.tahun}`,
            },
          },
          aggs: {
            jenisPajak: {
              terms: {
                field: "jenisPajak.keyword",
              },
              aggs: {
                npwpCompany: {
                  terms: {
                    field: "npwpCompany.keyword",
                  },
                  aggs: {
                    status: {
                      terms: {
                        field: "status.keyword",
                      },
                      aggs: {
                        jumlah: {
                          sum: {
                            field: "jumlah",
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  };
  if (req.query.npwp && req.query.jenisPajak) {
    for (let item of req.query.npwp.split(",")) {
      let obj = {
        bool: {
          must: [
            {
              match_phrase: {
                namaMenu: req.query.namaMenu,
              },
            },
            {
              match_phrase: {
                npwpCompany: item,
              },
            },
            {
              match_phrase: {
                jenisPajak: req.query.jenisPajak,
              },
            },
            {
              match_phrase: {
                "status.keyword": req.query.status,
              },
            },
          ],
        },
      };
      query.body.query.bool.should.push(obj);
    }
  }
  if (req.query.tahun) {
    let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
      req.query;
    let obj = {};
    rentangWaktu = "periode";
    obj[rentangWaktu] = {
      gte: rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
      lte:
        rentangWaktu === "periode"
          ? `${tahun}-${akhirbulan || awalbulan || "12"}`
          : tahun,
    };
    query.body.query.bool.filter.push({
      range: obj,
    });
  }

  return esClient.search(query);
};

let getDataChartBKWPTahunBanding = async (req) => {
  let query = {
    index: "businessintelligence*",
    type: "_doc",
    body: {
      sort: { periode: "asc" },
      query: {
        bool: {
          should: [],
          minimum_should_match: 1,
          filter: [],
        },
      },
      aggs: {
        total_count: {
          date_histogram: {
            field: "periode",
            calendar_interval: req.query.rentangWaktu === "tahun" ? "1y" : "1M",
            format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
            min_doc_count: 0,
            extended_bounds: {
              min:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                  : `${req.query.tahunbanding}`,
              max:
                req.query.rentangWaktu === "periode"
                  ? `${req.query.tahunbanding}-${
                      req.query.akhirbulan || req.query.awalbulan
                    }`
                  : `${req.query.tahunbanding}`,
            },
          },
          aggs: {
            jenisPajak: {
              terms: {
                field: "jenisPajak.keyword",
              },
              aggs: {
                npwpCompany: {
                  terms: {
                    field: "npwpCompany.keyword",
                  },
                  aggs: {
                    status: {
                      terms: {
                        field: "status.keyword",
                      },
                      aggs: {
                        jumlah: {
                          sum: {
                            field: "jumlah",
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  };
  if (req.query.npwp && req.query.jenisPajak) {
    for (let item of req.query.npwp.split(",")) {
      let obj = {
        bool: {
          must: [
            {
              match_phrase: {
                namaMenu: req.query.namaMenu,
              },
            },
            {
              match_phrase: {
                npwpCompany: item,
              },
            },
            {
              match_phrase: {
                jenisPajak: req.query.jenisPajak,
              },
            },
            {
              match_phrase: {
                "status.keyword": req.query.status,
              },
            },
          ],
        },
      };
      query.body.query.bool.should.push(obj);
    }
  }
  if (req.query.tahunbanding) {
    let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
      req.query;
    let obj = {};
    rentangWaktu = "periode";
    obj[rentangWaktu] = {
      gte:
        rentangWaktu === "periode"
          ? `${tahunbanding}-${awalbulan || "01"}`
          : tahunbanding,
      lte:
        rentangWaktu === "periode"
          ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
          : tahunbanding,
    };
    query.body.query.bool.filter.push({
      range: obj,
    });
  }

  return esClient.search(query);
};

// Search data summary
let getDataSummary = async (req) => {
  if (
    req.query.jenisPajak === `Jumlah ssp generate billing` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui BNIDirect` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui MPN Pajakku` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui CMS BRI` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui MCM Mandiri`
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              jenisPajak: {
                terms: {
                  field: "jenisPajak.keyword",
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      sum_data: {
                        sum: {
                          field: "jumlahSsp",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        query.body.query.bool.should.push({
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
            ],
          },
        });
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun || tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === `Jumlah terkait Konfirmasi` ||
    req.query.jenisPajak === `Jumlah terkait Validasi`
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              jenisPajak: {
                terms: {
                  field: "jenisPajak.keyword",
                },
                aggs: {
                  npwpCompany: {
                    terms: {
                      field: "npwpCompany.keyword",
                    },
                    aggs: {
                      status: {
                        terms: {
                          field: "status.keyword",
                        },
                        aggs: {
                          sum_data: {
                            sum: {
                              field: "jumlah",
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        query.body.query.bool.should.push({
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwpCompany: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  "status.keyword": req.query.status,
                },
              },
            ],
          },
        });
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === `Jumlah pembayaran melalui BNIDirect (Rupiah)` ||
    req.query.jenisPajak === `Jumlah pembayaran melalui CMS BRI (Rupiah)` ||
    req.query.jenisPajak === `Jumlah pembayaran melalui MPN Pajakku (Rupiah)` ||
    req.query.jenisPajak === `Jumlah pembayaran melalui MCM Mandiri (Rupiah)` ||
    req.query.jenisPajak === `Jumlah amount generate billing (Rupiah)`
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun || req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              jenisPajak: {
                terms: {
                  field: "jenisPajak.keyword",
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      sum_data: {
                        sum: {
                          field: "jumlahTransaksi",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        query.body.query.bool.should.push({
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
            ],
          },
        });
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataSummaryTahunBanding = async (req) => {
  if (
    req.query.jenisPajak === `Jumlah ssp generate billing` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui BNIDirect` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui MPN Pajakku` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui CMS BRI` ||
    req.query.jenisPajak === `Jumlah SSP pembayaran melalui MCM Mandiri`
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding || req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              jenisPajak: {
                terms: {
                  field: "jenisPajak.keyword",
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      sum_data: {
                        sum: {
                          field: "jumlahSsp",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        query.body.query.bool.should.push({
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
            ],
          },
        });
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding || tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === `Jumlah terkait Konfirmasi` ||
    req.query.jenisPajak === `Jumlah terkait Validasi`
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              jenisPajak: {
                terms: {
                  field: "jenisPajak.keyword",
                },
                aggs: {
                  npwpCompany: {
                    terms: {
                      field: "npwpCompany.keyword",
                    },
                    aggs: {
                      status: {
                        terms: {
                          field: "status.keyword",
                        },
                        aggs: {
                          sum_data: {
                            sum: {
                              field: "jumlah",
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        query.body.query.bool.should.push({
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwpCompany: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
              {
                match_phrase: {
                  "status.keyword": req.query.status,
                },
              },
            ],
          },
        });
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === `Jumlah pembayaran melalui BNIDirect (Rupiah)` ||
    req.query.jenisPajak === `Jumlah pembayaran melalui CMS BRI (Rupiah)` ||
    req.query.jenisPajak === `Jumlah pembayaran melalui MPN Pajakku (Rupiah)` ||
    req.query.jenisPajak === `Jumlah pembayaran melalui MCM Mandiri (Rupiah)` ||
    req.query.jenisPajak === `Jumlah amount generate billing (Rupiah)`
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding || req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              jenisPajak: {
                terms: {
                  field: "jenisPajak.keyword",
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      sum_data: {
                        sum: {
                          field: "jumlahTransaksi",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        query.body.query.bool.should.push({
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: req.query.jenisPajak,
                },
              },
            ],
          },
        });
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding || tahun}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataSummaryPPH = async (req) => {
  if (
    req.query.jenisPajak === "Jumlah Setor Sendiri" ||
    req.query.jenisPajak === "Jumlah Bukti Potong"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      pembetulan: {
                        terms: {
                          field: "pembetulan",
                          size: 1,
                          order: { _key: "desc" },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak === "Jumlah Setor Sendiri"
          ? { namaMenu: req.query.namaMenu, fg: 1 }
          : { namaMenu: req.query.namaMenu, fg: 0 }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak ===
      "Perbandingan PPh terutang Per Kategori (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang (Rupiah)" ||
    req.query.jenisPajak === "Perbandingan KB/LB (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak ===
          "Perbandingan PPh terutang Per Kategori (Rupiah)"
          ? { kategori: req.query.kategori }
          : req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
          ? { kodemap: req.query.kodeMap }
          : { namaMenu: req.query.namaMenu }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}-01`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  pembetulan: {
                    terms: {
                      field: "pembetulan",
                    },
                    aggs: {
                      tanggal: {
                        terms: { field: "tanggalLapor", format: "dd" },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPH",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: "Tanggal Lapor",
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataSummaryPPHTahunBanding = async (req) => {
  if (
    req.query.jenisPajak === "Jumlah Setor Sendiri" ||
    req.query.jenisPajak === "Jumlah Bukti Potong"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,

              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,

                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}`,
                  },
                },
                aggs: {
                  npwp: {
                    terms: {
                      field: "npwp.keyword",
                    },
                    aggs: {
                      pembetulan: {
                        terms: {
                          field: "pembetulan",
                          size: 1,
                          order: { _key: "desc" },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak === "Jumlah Setor Sendiri"
          ? { namaMenu: req.query.namaMenu, fg: 1 }
          : { namaMenu: req.query.namaMenu, fg: 0 }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak ===
      "Perbandingan PPh terutang Per Kategori (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang (Rupiah)" ||
    req.query.jenisPajak === "Perbandingan KB/LB (Rupiah)" ||
    req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      const clp = await Kategori.find(
        req.query.jenisPajak ===
          "Perbandingan PPh terutang Per Kategori (Rupiah)"
          ? { kategori: req.query.kategori }
          : req.query.jenisPajak === "Jumlah PPh terutang per Kode MAP (Rupiah)"
          ? { kodemap: req.query.kodeMap }
          : { namaMenu: req.query.namaMenu }
      );
      for (let item of req.query.npwp.split(",")) {
        for (let itemKop of clp) {
          let obj = {
            bool: {
              must: [
                {
                  match_phrase: {
                    namaMenu: "PPH",
                  },
                },
                {
                  match_phrase: {
                    npwp: item,
                  },
                },
                {
                  match_phrase: {
                    jenisPajak:
                      "Perbandingan PPh terutang Per Kategori (Rupiah)",
                  },
                },
                {
                  match_phrase: {
                    kodeMap: itemKop.kop,
                  },
                },
              ],
            },
          };
          query.body.query.bool.should.push(obj);
        }
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding || req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  pembetulan: {
                    terms: {
                      field: "pembetulan",
                    },
                    aggs: {
                      tanggal: {
                        terms: { field: "tanggalLapor", format: "dd" },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPH",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: "Tanggal Lapor",
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataSummaryPPN = async (req) => {
  if (req.query.jenisPajak === "Jumlah PPN Terutang") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.kodeMap ===
                                    "PPN Dalam Negeri Setoran Masa"
                                      ? "lampiranInduk.jmlIIDPPN"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Kegiatan Membangun Sendiri"
                                      ? "lampiranInduk.jmlIIIB"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Pembayaran Kembali Pajak Masukan Bagi Pkp Gagal Berproduksi"
                                      ? "lampiranInduk.jmlIVA"
                                      : "lampiranInduk.jmlVCPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Perbandingan KB/LB") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "lampiranInduk.jmlIIDPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              sum_b1: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB1PPN",
                                },
                              },
                              sum_b2: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB2PPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === "Jumlah PPN terkait impor terutang" ||
    req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${req.query.awalbulan}`
                        : `${req.query.tahun}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahun}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahun}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.jenisPajak ===
                                    "Jumlah PPN terkait impor terutang"
                                      ? "lampiranB1.jmlIIAPPN"
                                      : "lampiranA1.jmlIADPP",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || 12}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  pembetulan: {
                    terms: {
                      field: "pembetulan",
                    },
                    aggs: {
                      tanggal: {
                        terms: { field: "tanggalLapor", format: "dd" },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: "Tanggal Lapor",
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode" ? `${tahun}-${awalbulan || "01"}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan || "12"}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Bayar") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${req.query.awalbulan}`
                    : `${req.query.tahun}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahun}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahun}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  pembetulan: {
                    terms: {
                      field: "pembetulan",
                      size: 1,
                      order: { _key: "desc" },
                    },
                    aggs: {
                      tanggal: {
                        terms: {
                          field: "tanggalBayar",
                          format: "dd",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte: rentangWaktu === "periode" ? `${tahun}-${awalbulan}` : tahun,
        lte:
          rentangWaktu === "periode"
            ? `${tahun}-${akhirbulan || awalbulan}`
            : tahun,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

let getDataSummaryPPNTahunBanding = async (req) => {
  if (req.query.jenisPajak === "Jumlah PPN Terutang") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "jumlahPphPpn",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.kodeMap ===
                                    "PPN Dalam Negeri Setoran Masa"
                                      ? "lampiranInduk.jmlIIDPPN"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Kegiatan Membangun Sendiri"
                                      ? "lampiranInduk.jmlIIIB"
                                      : req.query.kodeMap ===
                                        "PPN Terutang Atas Pembayaran Kembali Pajak Masukan Bagi Pkp Gagal Berproduksi"
                                      ? "lampiranInduk.jmlIVA"
                                      : "lampiranInduk.jmlVCPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Perbandingan KB/LB") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field: "lampiranInduk.jmlIIDPPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              sum_b1: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB1PPN",
                                },
                              },
                              sum_b2: {
                                sum: {
                                  field: "lampiranAB.jmlIIIB2PPN",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (
    req.query.jenisPajak === "Jumlah PPN terkait impor terutang" ||
    req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang"
  ) {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_pertahun: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              data_perbulan: {
                date_histogram: {
                  field: "periode",
                  calendar_interval: "1M",
                  format: "yyyy-MM",
                  min_doc_count: 0,
                  extended_bounds: {
                    min:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                        : `${req.query.tahunbanding}-01`,
                    max:
                      req.query.rentangWaktu === "periode"
                        ? `${req.query.tahunbanding}-${
                            req.query.akhirbulan || req.query.awalbulan
                          }`
                        : `${req.query.tahunbanding}-12`,
                  },
                },
                aggs: {
                  jenisPajak: {
                    terms: {
                      field: "jenisPajak.keyword",
                    },
                    aggs: {
                      npwp: {
                        terms: {
                          field: "npwp.keyword",
                        },
                        aggs: {
                          pembetulan: {
                            terms: {
                              field: "pembetulan",
                              size: 1,
                              order: { _key: "desc" },
                            },
                            aggs: {
                              jumlah: {
                                sum: {
                                  field:
                                    req.query.jenisPajak ===
                                    "Jumlah PPN terkait impor terutang"
                                      ? "lampiranB1.jmlIIAPPN"
                                      : "lampiranA1.jmlIADPP",
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: req.query.namaMenu,
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahun) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || 12}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Lapor") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        sort: { periode: "asc" },
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval:
                req.query.rentangWaktu === "tahun" ? "1y" : "1M",
              format: req.query.rentangWaktu === "tahun" ? "yyyy" : "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  pembetulan: {
                    terms: {
                      field: "pembetulan",
                    },
                    aggs: {
                      tanggal: {
                        terms: { field: "tanggalLapor", format: "dd" },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  jenisPajak: "Tanggal Lapor",
                },
              },
              {
                match_phrase: {
                  pembetulan: 0,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      rentangWaktu = "periode";
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan || "01"}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan || "12"}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
  if (req.query.jenisPajak === "Tanggal Bayar") {
    let query = {
      index: "businessintelligence*",
      type: "_doc",
      body: {
        query: {
          bool: {
            should: [],
            minimum_should_match: 1,
            filter: [],
          },
        },
        aggs: {
          data_perbulan: {
            date_histogram: {
              field: "periode",
              calendar_interval: "1M",
              format: "yyyy-MM",
              min_doc_count: 0,
              extended_bounds: {
                min:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${req.query.awalbulan}`
                    : `${req.query.tahunbanding}`,
                max:
                  req.query.rentangWaktu === "periode"
                    ? `${req.query.tahunbanding}-${
                        req.query.akhirbulan || req.query.awalbulan
                      }`
                    : `${req.query.tahunbanding}`,
              },
            },
            aggs: {
              npwp: {
                terms: {
                  field: "npwp.keyword",
                },
                aggs: {
                  pembetulan: {
                    terms: {
                      field: "pembetulan",
                      size: 1,
                      order: { _key: "desc" },
                    },
                    aggs: {
                      tanggal: {
                        terms: {
                          field: "tanggalBayar",
                          format: "dd",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
    if (req.query.npwp && req.query.jenisPajak) {
      for (let item of req.query.npwp.split(",")) {
        let obj = {
          bool: {
            must: [
              {
                match_phrase: {
                  namaMenu: "PPN",
                },
              },
              {
                match_phrase: {
                  npwp: item,
                },
              },
              {
                match_phrase: {
                  "jenisPajak.keyword": req.query.jenisPajak,
                },
              },
            ],
          },
        };
        query.body.query.bool.should.push(obj);
      }
    }
    if (req.query.tahunbanding) {
      let { tahun, tahunbanding, awalbulan, akhirbulan, rentangWaktu } =
        req.query;
      let obj = {};
      obj[rentangWaktu] = {
        gte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${awalbulan}`
            : tahunbanding,
        lte:
          rentangWaktu === "periode"
            ? `${tahunbanding}-${akhirbulan || awalbulan}`
            : tahunbanding,
      };
      query.body.query.bool.filter.push({
        range: obj,
      });
    }
    return esClient.search(query);
  }
};

module.exports = {
  getDataPph,
  getDataSummary,
  getDataChartEBilling,
  getDataBKWP,
  getDataChartBKWP,
  getDataPph42,
  getDataChartPPH4,
  getDataSummaryPPH,
  getDataPPN,
  getDataChartPPN,
  getDataSummaryPPN,
  getDataChartPPNTahunBanding,
  getDataSummaryPPNTahunBanding,
  getDataSummaryTahunBanding,
  getDataChartEBillingTahunBanding,
  getDataChartBKWPTahunBanding,
  getDataSummaryPPHTahunBanding,
  getDataChartPPH4TahunBanding,
};
