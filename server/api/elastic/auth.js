import jwt from 'jsonwebtoken';
import compose from 'composable-middleware';
import config from '../../../config/environment';


export const decodeProfile = (method) => {
    return compose()
      .use(async (req, res, next) => {
        if(config.enabledAuth){
          let xPajakkuProfile = req.headers['x-profile'];
          if(!xPajakkuProfile) return res.status(401).send('Forbidden. Pajakku profile is not found')
          try{
            const decoded = await jwt.verify(xPajakkuProfile, config.sharedKey, {
              algorithms:["HS256"]
            });
            req.user = decoded.metadata;
            if(req.body && ['post', 'put'].indexOf(method) >= 0){
              if(method === 'post'){
                req.body.createdBy = req.user.username;
                req.body.dateCreated = new Date();
                delete req.body.__id;
              };
              if(method === 'put'){
                req.body.updatedBy = req.user.username;
                req.body.dateModified = new Date();
                //delete req.body.published;
                delete req.body.datePublished;
              }
            }
            next();
          } catch (err) {
            return res.status(500).send(err);
          }
        } else {
          req.body.createdBy = 'disabled-auth';
          req.user = {
            username: 'disabled-auth',
            email: 'disabled@auth.com',
            name: 'Disabled Auth'
          }
          next();
        }
      })
  }
  