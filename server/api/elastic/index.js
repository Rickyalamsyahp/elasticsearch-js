/* eslint-disable no-mixed-operators */
/* eslint-disable no-redeclare */
"use strict";
import { find, groupBy } from "lodash";
import moment from "moment";
import wrapAsync from "../../services/wrap.async";
import _ from "lodash";
import * as auth from './auth.js';

const elastic = require("./elastic.controller");
const router = require("express").Router();
const mongodb = require("./mongodb");


// GET Data
router.get(
  "/pph",
  wrapAsync(async (req, res) => {
    // let totalData;
    if (
      req.query.namaMenu === "PPH4/2" ||
      req.query.namaMenu === "PPH23/26" ||
      req.query.namaMenu === "PPH15" ||
      req.query.namaMenu === "PPH22" ||
      req.query.namaMenu === "PPH21"
    ) {
      const data = await elastic.getDataPph42(req);
      var _data = [];
      for (var i = 0; i < data.aggregations.jenisPajak.buckets.length; i++) {
        const dataNamaMenu = data.aggregations.jenisPajak.buckets[i];
        for (let a = 0; a < dataNamaMenu.npwp.buckets.length; a++) {
          const dataNpwp = dataNamaMenu.npwp.buckets[a];
          for (let b = 0; b < dataNpwp.kategori.buckets.length; b++) {
            const dataKategori = dataNpwp.kategori.buckets[b];
            for (let c = 0; c < dataKategori.kodemap.buckets.length; c++) {
              const dataKodeMap = dataKategori.kodemap.buckets[c];
              _data.push({
                npwp: dataNpwp.key,
                kategori: dataKategori.key,
                kodeMap: dataKodeMap.key,
              });
            }
          }
        }
      }
      return _data;
    }
    if (req.query.namaMenu === "PPN") {
      const data = await elastic.getDataPPN(req);
      var _data = [];
      for (let a = 0; a < data.aggregations.jenisPajak.buckets.length; a++) {
        const dataJenisPajak = data.aggregations.jenisPajak.buckets[a];
        _data.push({
          jenisPajak: dataJenisPajak.key,
        });
      }
      return _data;
    }
    if (req.query.namaMenu === "BKWP") {
      const data = await elastic.getDataBKWP(req);
      var _data = [];
      for (let a = 0; a < data.aggregations.jenisPajak.buckets.length; a++) {
        const element = data.aggregations.jenisPajak.buckets[a];
        for (let b = 0; b < element.npwpCompany.buckets.length; b++) {
          const dataNpwpCompany = element.npwpCompany.buckets[b];
          for (let c = 0; c < dataNpwpCompany.status.buckets.length; c++) {
            const dataStatus = dataNpwpCompany.status.buckets[c];
            _data.push({
              jenisPajak: element.key,
              npwp: dataNpwpCompany.key,
              status: dataStatus.key,
            });
          }
        }
      }
      return _data;
    } else {
      const data = await elastic.getDataPph(req);
      var _data = [];
      for (var i = 0; i < data.aggregations.jenisPajak.buckets.length; i++) {
        var dataNamaMenu = data.aggregations.jenisPajak.buckets[i];
        for (let k = 0; k < dataNamaMenu.npwp.buckets.length; k++) {
          var dataNpwp = dataNamaMenu.npwp.buckets[k];
          if (dataNpwp.kategori.buckets.length > 0) {
            for (let q = 0; q < dataNpwp.kategori.buckets.length; q++) {
              var dataKategori = dataNpwp.kategori.buckets[q];
              for (let m = 0; m < dataKategori.kodemap.buckets.length; m++) {
                var dataKodeMap = dataKategori.kodemap.buckets[m];
                _data.push({
                  jenisPajak: dataNamaMenu.key,
                  npwp: dataNpwp.key,
                  kodeMap: dataKodeMap.key,
                  kategori: dataKategori.key,
                });
              }
            }
          } else {
            _data.push({
              jenisPajak: dataNamaMenu.key,
              npwp: dataNpwp.key,
            });
          }
        }
      }
      return _data;
    }
  })
);

router.get(
  "/chart",
  wrapAsync(async (req, res) => {
    if (req.query.namaMenu === "PPN" && req.query.tahunbanding) {
      if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
        const data = await elastic.getDataChartPPN(req);
        const data2 = await elastic.getDataChartPPNTahunBanding(req);
        var _data_ = [];
        var dataPertahun = [];
        var dataperbulan = [];
        var datapertahun2 = [];
        var dataperbulan2 = [];
        var _data_ = [];
        var _data_ = [];
        var _data2_ = [];
        var _data__ = [];
        var _data2__ = [];
        var dataFinal = [];
        var dataFinal2 = [];
        // var _datasss = [];
        // var kata_ = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn:
                      dataPembetulan.sum_b1.value + dataPembetulan.sum_b2.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          datapertahun2.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan2.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data2_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn:
                      dataPembetulan.sum_b1.value + dataPembetulan.sum_b2.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPphPpn = find(_data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
        }
        for (let a = 0; a < datapertahun2.length; a++) {
          const element = datapertahun2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPphPpn = find(_data2_, {
              key: element.key,
              category: test,
            });

            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
        }

        for (let item of dataFinal) {
          let { key, category } = item;

          let month = key.split("-")[1];

          let itemCompare = find(dataFinal2, {
            key: `${req.query.tahunbanding}-${month}`,
            category: category,
          });
          item.lastJumlahPpn = itemCompare ? itemCompare.jumlahPphPpn : 0;
        }

        return dataFinal;
      }
      if (
        req.query.jenisPajak === "Jumlah PPN Terutang" ||
        req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP" ||
        req.query.jenisPajak === "Perbandingan KB/LB" ||
        req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang" ||
        req.query.jenisPajak === "Jumlah PPN terkait impor terutang"
      ) {
        const data = await elastic.getDataChartPPN(req);
        const data2 = await elastic.getDataChartPPNTahunBanding(req);
        var _data_ = [];
        var dataPertahun = [];
        var dataperbulan = [];
        var datapertahun2 = [];
        var dataperbulan2 = [];
        var _data_ = [];
        var _data_ = [];
        var _data2_ = [];
        var _data__ = [];
        var _data2__ = [];
        var dataFinal = [];
        var dataFinal2 = [];

        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          dataPertahun.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    // jenisPajak: dataJenisPajak.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          datapertahun2.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan2.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data2_.push({
                    category: dataNpwp.key,
                    // jenisPajak: dataJenisPajak.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        for (let a = 0; a < dataPertahun.length; a++) {
          const element = dataPertahun[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
        }
        for (let a = 0; a < datapertahun2.length; a++) {
          const element = datapertahun2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data2_, {
              key: element.key,
              category: test,
            });
            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
        }
        for (let item of dataFinal) {
          let { key, category } = item;

          let month = key.split("-")[1];

          let itemCompare = find(dataFinal2, {
            key: `${req.query.tahunbanding}-${month}`,
            category: category,
          });
          item.lastJumlahPpn = itemCompare ? itemCompare.jumlahPphPpn : 0;
        }
        return dataFinal;
      }
    }
    if (req.query.namaMenu === "EBilling" && req.query.tahunbanding) {
      const data = await elastic.getDataChartEBilling(req);
      const data2 = await elastic.getDataChartEBillingTahunBanding(req);
      var _data = [];
      var _data__ = [];
      var dataFinal = [];
      var _data2 = [];
      var data2_ = [];
      var dataEbiling2 = [];
      // var kata = ["Total", "Terbesar", "Terkecil"];
      for (let a = 0; a < data.aggregations.total_count.buckets.length; a++) {
        const dataTotalCount = data.aggregations.total_count.buckets[a];
        _data__.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwp.buckets[c];
            _data.push({
              category: dataNpwp.key,
              key: dataTotalCount.key_as_string,
              jumlahTransaksi: dataNpwp.jumlah.value,
              jumlahSsp: dataNpwp.jumlahSsp.value,
              // label: kata[0],
            });
          }
        }
      }

      for (let a = 0; a < data2.aggregations.total_count.buckets.length; a++) {
        const dataTotalCount = data2.aggregations.total_count.buckets[a];
        _data2.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwp.buckets[c];
            data2_.push({
              category: dataNpwp.key,
              key: dataTotalCount.key_as_string,
              jumlahTransaksi: dataNpwp.jumlah.value,
              jumlahSsp: dataNpwp.jumlahSsp.value,
              // label: kata[0],
            });
          }
        }
      }

      for (let a = 0; a < _data__.length; a++) {
        const element = _data__[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahEbilling = find(_data, {
            key: element.key,
            category: test,
          });
          dataFinal.push({
            key: element.key,
            category: test,
            jumlahTransaksi: jumlahEbilling
              ? jumlahEbilling.jumlahTransaksi
              : 0,
            jumlahSsp: jumlahEbilling ? jumlahEbilling.jumlahSsp : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
      }

      for (let a = 0; a < _data2.length; a++) {
        const element = _data2[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahEbilling = find(data2_, {
            key: element.key,
            category: test,
          });
          dataEbiling2.push({
            key: element.key,
            category: test,
            jumlahTransaksi: jumlahEbilling
              ? jumlahEbilling.jumlahTransaksi
              : 0,
            jumlahSsp: jumlahEbilling ? jumlahEbilling.jumlahSsp : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
      }

      for (let item of dataFinal) {
        let { key, category } = item;

        let month = key.split("-")[1];

        let itemCompare = find(dataEbiling2, {
          key: `${req.query.tahunbanding}-${month}`,
          category: category,
        });
        item.lastJumlahTransaksi = itemCompare
          ? itemCompare.jumlahTransaksi
          : 0;
        item.lastJumlahSsp = itemCompare ? itemCompare.jumlahSsp : 0;
      }

      return dataFinal;
    }
    if (req.query.namaMenu === "EBilling") {
      const data = await elastic.getDataChartEBilling(req);
      var _data = [];
      var _data__ = [];
      var dataFinal = [];
      // var kata = ["Total", "Terbesar", "Terkecil"];
      for (let a = 0; a < data.aggregations.total_count.buckets.length; a++) {
        const dataTotalCount = data.aggregations.total_count.buckets[a];
        _data__.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwp.buckets[c];
            _data.push({
              category: dataNpwp.key,
              key: dataTotalCount.key_as_string,
              jumlahTransaksi: dataNpwp.jumlah.value,
              jumlahSsp: dataNpwp.jumlahSsp.value,
              // label: kata[0],
            });
          }
        }
      }
      for (let a = 0; a < _data__.length; a++) {
        const element = _data__[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahEbilling = find(_data, {
            key: element.key,
            category: test,
          });
          dataFinal.push({
            key: element.key,
            category: test,
            jumlahTransaksi: jumlahEbilling
              ? jumlahEbilling.jumlahTransaksi
              : 0,
            jumlahSsp: jumlahEbilling ? jumlahEbilling.jumlahSsp : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
      }
      return dataFinal;
    }
    if (req.query.namaMenu === "BKWP" && req.query.tahunbanding) {
      const data = await elastic.getDataChartBKWP(req);
      const data2 = await elastic.getDataChartBKWPTahunBanding(req);
      var _data = [];
      var _data__ = [];
      var dataFinal = [];
      var _databwkp2 = [];
      var _databwkp2__ = [];
      var dataFinalbwkp2 = [];
      // var kata = ["Total", "Terbesar", "Terkecil"];
      for (let a = 0; a < data.aggregations.total_count.buckets.length; a++) {
        const dataTotalCount = data.aggregations.total_count.buckets[a];
        _data__.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwpCompany.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwpCompany.buckets[c];
            for (let d = 0; d < dataNpwp.status.buckets.length; d++) {
              const dataStatus = dataNpwp.status.buckets[d];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                // jenisPajak: dataJenisPajak.key,
                jumlah: dataStatus.jumlah.value,
                // label: kata[0],
              });
            }
          }
        }
      }

      for (let a = 0; a < data2.aggregations.total_count.buckets.length; a++) {
        const dataTotalCount = data2.aggregations.total_count.buckets[a];
        _databwkp2.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwpCompany.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwpCompany.buckets[c];
            for (let d = 0; d < dataNpwp.status.buckets.length; d++) {
              const dataStatus = dataNpwp.status.buckets[d];
              _databwkp2__.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                // jenisPajak: dataJenisPajak.key,
                jumlah: dataStatus.jumlah.value,
                // label: kata[0],
              });
            }
          }
        }
      }

      for (let a = 0; a < _data__.length; a++) {
        const element = _data__[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahBkwp = find(_data, {
            key: element.key,
            category: test,
          });
          dataFinal.push({
            key: element.key,
            category: test,
            jumlah: jumlahBkwp ? jumlahBkwp.jumlah : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
      }

      for (let a = 0; a < _databwkp2.length; a++) {
        const element = _databwkp2[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahBkwp = find(_databwkp2__, {
            key: element.key,
            category: test,
          });
          dataFinalbwkp2.push({
            key: element.key,
            category: test,
            jumlah: jumlahBkwp ? jumlahBkwp.jumlah : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
      }

      for (let item of dataFinal) {
        let { key, category } = item;

        let month = key.split("-")[1];

        let itemCompare = find(dataFinalbwkp2, {
          key: `${req.query.tahunbanding}-${month}`,
          category: category,
        });
        item.lastJumlah = itemCompare ? itemCompare.jumlah : 0;
      }

      return dataFinal;
    }
    if (req.query.namaMenu === "BKWP") {
      const data = await elastic.getDataChartBKWP(req);

      var _data = [];
      var _data__ = [];
      var dataFinal = [];

      // var kata = ["Total", "Terbesar", "Terkecil"];
      for (let a = 0; a < data.aggregations.total_count.buckets.length; a++) {
        const dataTotalCount = data.aggregations.total_count.buckets[a];
        _data__.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwpCompany.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwpCompany.buckets[c];
            for (let d = 0; d < dataNpwp.status.buckets.length; d++) {
              const dataStatus = dataNpwp.status.buckets[d];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                // jenisPajak: dataJenisPajak.key,
                jumlah: dataStatus.jumlah.value,
                // label: kata[0],
              });
            }
          }
        }
      }

      for (let a = 0; a < _data__.length; a++) {
        const element = _data__[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahBkwp = find(_data, {
            key: element.key,
            category: test,
          });
          dataFinal.push({
            key: element.key,
            category: test,
            jumlah: jumlahBkwp ? jumlahBkwp.jumlah : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
      }

      return dataFinal;
    }
    if (
      (req.query.namaMenu === "PPH4/2" && req.query.tahunbanding) ||
      (req.query.namaMenu === "PPH23/26" && req.query.tahunbanding) ||
      (req.query.namaMenu === "PPH22" && req.query.tahunbanding)
    ) {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataChartPPH4(req);
        const data2 = await elastic.getDataChartPPH4TahunBanding(req);
        var data_ = [];
        var _data__ = [];
        var dataFinal = [];
        var data2_ = [];
        var _data2__ = [];
        var dataFinal2 = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataPerbulan = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataPerbulan.key_as_string,
          });
          for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
            const datanpwp = dataPerbulan.npwp.buckets[b];
            for (let c = 0; c < datanpwp.tanggal.buckets.length; c++) {
              const dataTanggal = datanpwp.tanggal.buckets[c];
              data_.push({
                category: datanpwp.key,
                key: dataPerbulan.key_as_string,
                tanggalLapor: dataTanggal.key_as_string,
                // label: kata[0],
                // category2: "Batas Tanggal Lapor",
                // batasTanggalLapor: "20",
              });
            }
          }
        }

        for (
          let a = 0;
          a < data2.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataPerbulan = data2.aggregations.data_perbulan.buckets[a];
          data2_.push({
            key: dataPerbulan.key_as_string,
          });
          for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
            const datanpwp = dataPerbulan.npwp.buckets[b];
            for (let c = 0; c < datanpwp.tanggal.buckets.length; c++) {
              const dataTanggal = datanpwp.tanggal.buckets[c];
              _data2__.push({
                category: datanpwp.key,
                key: dataPerbulan.key_as_string,
                tanggalLapor: dataTanggal.key_as_string,
                // label: kata[0],
                // category2: "Batas Tanggal Lapor",
                // batasTanggalLapor: "20",
              });
            }
          }
        }

        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const tanggalLapor = find(data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              tanggalLapor: tanggalLapor ? tanggalLapor.tanggalLapor : "0",
              category2: "Batas Tanggal Lapor",
              batasTanggalLapor: "20",
            });
          }
        }

        for (let a = 0; a < data2_.length; a++) {
          const element = data2_[a];
          for (let test of req.query.npwp.split(",")) {
            const tanggalLapor = find(_data2__, {
              key: element.key,
              category: test,
            });
            dataFinal2.push({
              key: element.key,
              category: test,
              tanggalLapor: tanggalLapor ? tanggalLapor.tanggalLapor : "0",
              category2: "Batas Tanggal Lapor",
              batasTanggalLapor: "20",
            });
          }
        }

        for (let item of dataFinal) {
          let { key, category } = item;

          let month = key.split("-")[1];

          let itemCompare = find(dataFinal2, {
            key: `${req.query.tahunbanding}-${month}`,
            category: category,
          });
          item.lastTanggalLapor = itemCompare ? itemCompare.tanggalLapor : 0;
        }
        return dataFinal;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataChartPPH4(req);
        const data2 = await elastic.getDataChartPPH4TahunBanding(req);
        var _data = [];
        var _data_ = [];
        var _data2 = [];
        var _data2_ = [];
        var totaldata = [];
        var _data2 = [];
        var totaldata2 = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data_.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data2_.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (let a = 0; a < _data_.length; a++) {
          const element = _data_[a];
          // for (let b = 0; b < _dataJenisPajak.length; b++) {
          //   const dataJenisPajak = _dataJenisPajak[b];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data, {
              key: element.key,
              category: test,
            });
            totaldata.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
        }
        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data2_, {
              key: element.key,
              category: test,
            });
            totaldata2.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
            });
          }
        }

        for (let item of totaldata) {
          let { key, category } = item;

          let month = key.split("-")[1];

          let itemCompare = find(totaldata2, {
            key: `${req.query.tahunbanding}-${month}`,
            category: category,
          });
          item.lastJumlahTransaksi = itemCompare
            ? itemCompare.jumlahTransaksi
            : 0;
        }
        return totaldata;
      } else {
        const data = await elastic.getDataChartPPH4(req);
        const data2 = await elastic.getDataChartPPH4TahunBanding(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var _data2 = [];
        var data2_ = [];
        var dataFinal2 = [];
        // var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  data2_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        //final data
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          // for (let b = 0; b < _dataJenisPajak.length; b++) {
          //   const dataJenisPajak = _dataJenisPajak[b];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
        }
        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(data2_, {
              key: element.key,
              category: test,
            });
            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
        }

        for (let item of dataFinal) {
          let { key, category } = item;

          let month = key.split("-")[1];

          let itemCompare = find(dataFinal2, {
            key: `${req.query.tahunbanding}-${month}`,
            category: category,
          });
          item.lastJumlahPphPpn = itemCompare ? itemCompare.jumlahPphPpn : 0;
        }
        return dataFinal;
      }
    }
    if (
      (req.query.namaMenu === "PPH15" && req.query.tahunbanding) ||
      (req.query.namaMenu === "PPH21" && req.query.tahunbanding)
    ) {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataChartPPH4(req);
        var data_ = [];
        var _data__ = [];
        var dataFinal = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataPerbulan = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataPerbulan.key_as_string,
          });
          for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
            const datanpwp = dataPerbulan.npwp.buckets[b];
            for (let c = 0; c < datanpwp.tanggal.buckets.length; c++) {
              const dataTanggal = datanpwp.tanggal.buckets[c];
              data_.push({
                category: datanpwp.key,
                key: dataPerbulan.key_as_string,
                tanggalLapor: dataTanggal.key_as_string,
                // label: kata[0],
                // category2: "Batas Tanggal Lapor",
                // batasTanggalLapor: "20",
              });
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const tanggalLapor = find(data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              tanggalLapor: tanggalLapor ? tanggalLapor.tanggalLapor : "0",
              category2: "Batas Tanggal Lapor",
              batasTanggalLapor: "20",
            });
          }
        }
        return dataFinal;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataChartPPH4(req);
        const data2 = await elastic.getDataChartPPH4TahunBanding(req);
        var _data = [];
        var _data_ = [];
        var _data2 = [];
        var _data2_ = [];
        var totaldata = [];
        var _data2 = [];
        var totaldata2 = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data_.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data2_.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (let a = 0; a < _data_.length; a++) {
          const element = _data_[a];
          // for (let b = 0; b < _dataJenisPajak.length; b++) {
          //   const dataJenisPajak = _dataJenisPajak[b];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data, {
              key: element.key,
              category: test,
            });
            totaldata.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
        }
        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data2_, {
              key: element.key,
              category: test,
            });
            totaldata2.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
            });
          }
        }

        for (let item of totaldata) {
          let { key, category } = item;

          let month = key.split("-")[1];

          let itemCompare = find(totaldata2, {
            key: `${req.query.tahunbanding}-${month}`,
            category: category,
          });
          item.lastJumlahTransaksi = itemCompare
            ? itemCompare.jumlahTransaksi
            : 0;
        }
        return totaldata;
      } else {
        const data = await elastic.getDataChartPPH4(req);
        const data2 = await elastic.getDataChartPPH4TahunBanding(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var _data2 = [];
        var data2_ = [];
        var dataFinal2 = [];
        // var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  data2_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        //final data
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          // for (let b = 0; b < _dataJenisPajak.length; b++) {
          //   const dataJenisPajak = _dataJenisPajak[b];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
        }
        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(data2_, {
              key: element.key,
              category: test,
            });
            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
        }

        for (let item of dataFinal) {
          let { key, category } = item;

          let month = key.split("-")[1];

          let itemCompare = find(dataFinal2, {
            key: `${req.query.tahunbanding}-${month}`,
            category: category,
          });
          item.lastJumlahPphPpn = itemCompare ? itemCompare.jumlahPphPpn : 0;
        }
        return dataFinal;
      }
    }
    if (
      req.query.namaMenu === "PPH4/2" ||
      req.query.namaMenu === "PPH23/26" ||
      req.query.namaMenu === "PPH22"
    ) {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataChartPPH4(req);
        var data_ = [];
        var _data__ = [];
        var dataFinal = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataPerbulan = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataPerbulan.key_as_string,
          });
          for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
            const datanpwp = dataPerbulan.npwp.buckets[b];
            for (let c = 0; c < datanpwp.tanggal.buckets.length; c++) {
              const dataTanggal = datanpwp.tanggal.buckets[c];
              data_.push({
                category: datanpwp.key,
                key: dataPerbulan.key_as_string,
                tanggalLapor: dataTanggal.key_as_string,
                // label: kata[0],
                // category2: "Batas Tanggal Lapor",
                // batasTanggalLapor: "20",
              });
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const tanggalLapor = find(data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              tanggalLapor: tanggalLapor ? tanggalLapor.tanggalLapor : "0",
              category2: "Batas Tanggal Lapor",
              batasTanggalLapor: "20",
            });
          }
        }
        return dataFinal;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataChartPPH4(req);
        var _data = [];
        var _data_ = [];
        var _datapertahun = [];
        var totaldata = [];
        var totaldataPerbulan = [];
        var totaldataPertahun = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _datapertahun.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            _data_.push({
              key: dataperbulan.key_as_string,
            });
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data.push({
                  category: dataNpwp.key,
                  key:
                    req.query.rentangWaktu === "tahun"
                      ? datapertahun.key_as_string
                      : dataperbulan.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahTransaksi);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahTransaksi: sum,
              label: kata[0],
            });
          }
          for (let index = 0; index < _datapertahun.length; index++) {
            const element = _datapertahun[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahTransaksi = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahTransaksi: jumlahTransaksi
                  ? jumlahTransaksi.jumlahTransaksi
                  : 0,
              });
            }
          }
        } else {
          for (let a = 0; a < _datapertahun.length; a++) {
            const element = _datapertahun[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahTransaksi = find(_data, {
                key: element.key,
                category: test,
              });
              totaldataPerbulan.push({
                key: element.key,
                category: test,
                jumlahTransaksi: jumlahTransaksi
                  ? jumlahTransaksi.jumlahTransaksi
                  : 0,
                // jenisPajak: dataJenisPajak.jenisPajak,
              });
            }
          }
        }
        return req.query.rentangWaktu === "tahun"
          ? totaldataPertahun
          : totaldataPerbulan;
      } else {
        const data = await elastic.getDataChartPPH4(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var totaldataPerbulan = [];
        var totaldata = [];
        var totaldataPertahun = [];
        // var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }

        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(_data, {
                key: element.key,
                category: test,
              });
              totaldataPerbulan.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
        }
        return req.query.rentangWaktu === "tahun"
          ? totaldataPertahun
          : totaldataPerbulan;
      }
    }
    if (req.query.namaMenu === "PPH15" || req.query.namaMenu === "PPH21") {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataChartPPH4(req);
        var data_ = [];
        var _data__ = [];
        var dataFinal = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataPerbulan = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataPerbulan.key_as_string,
          });
          for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
            const datanpwp = dataPerbulan.npwp.buckets[b];
            for (let c = 0; c < datanpwp.tanggal.buckets.length; c++) {
              const dataTanggal = datanpwp.tanggal.buckets[c];
              data_.push({
                category: datanpwp.key,
                key: dataPerbulan.key_as_string,
                tanggalLapor: dataTanggal.key_as_string,
                // label: kata[0],
                // category2: "Batas Tanggal Lapor",
                // batasTanggalLapor: "20",
              });
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const tanggalLapor = find(data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              tanggalLapor: tanggalLapor ? tanggalLapor.tanggalLapor : "0",
              category2: "Batas Tanggal Lapor",
              batasTanggalLapor: "20",
            });
          }
        }
        return dataFinal;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataChartPPH4(req);
        var _data = [];
        var _data_ = [];
        var _datapertahun = [];
        var totaldata = [];
        var totaldataPerbulan = [];
        var totaldataPertahun = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _datapertahun.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            _data_.push({
              key: dataperbulan.key_as_string,
            });
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data.push({
                  category: dataNpwp.key,
                  key:
                    req.query.rentangWaktu === "tahun"
                      ? datapertahun.key_as_string
                      : dataperbulan.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
          if (req.query.rentangWaktu === "tahun") {
            let groupNpwp = groupBy(_data, (d) => d.category);
            for (let key of Object.keys(groupNpwp)) {
              var tes = groupNpwp[key].map((a) => a.jumlahTransaksi);
              var testing = groupNpwp[key].map((a) => a.category);
              var sum = tes.reduce(function (a, b) {
                return a + b;
              }, 0);
              totaldata.push({
                category: testing.shift(),
                key: datapertahun.key_as_string,
                jumlahTransaksi: sum,
                label: kata[0],
              });
            }
          }
        }
        for (let index = 0; index < _datapertahun.length; index++) {
          const element = _datapertahun[index];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(totaldata, {
              key: element.key,
              category: test,
            });
            totaldataPertahun.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
            });
          }
        }
        for (let a = 0; a < _data_.length; a++) {
          const element = _data_[a];
          // for (let b = 0; b < _dataJenisPajak.length; b++) {
          //   const dataJenisPajak = _dataJenisPajak[b];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data, {
              key: element.key,
              category: test,
            });
            totaldataPerbulan.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
        }
        return req.query.rentangWaktu === "tahun"
          ? totaldataPertahun
          : totaldataPerbulan;
      } else {
        const data = await elastic.getDataChartPPH4(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var totaldataPerbulan = [];
        var totaldata = [];
        var totaldataPertahun = [];
        // var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }

        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(_data, {
                key: element.key,
                category: test,
              });
              totaldataPerbulan.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
                // jenisPajak: dataJenisPajak.jenisPajak,
              });
            }
          }
        }
        return req.query.rentangWaktu === "tahun"
          ? totaldataPertahun
          : totaldataPerbulan;
      }
    }
    if (req.query.namaMenu === "PPN") {
      if (req.query.jenisPajak === "Tanggal Bayar") {
        const data = await elastic.getDataChartPPN(req);
        var data_ = [];
        var _data__ = [];
        var dataFinal = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataPerbulan = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataPerbulan.key_as_string,
          });
          for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
            const datanpwp = dataPerbulan.npwp.buckets[b];
            for (let c = 0; c < datanpwp.tanggal.buckets.length; c++) {
              const dataTanggal = datanpwp.tanggal.buckets[c];
              data_.push({
                category: datanpwp.key,
                key: dataPerbulan.key_as_string,
                tanggalBayar: dataTanggal.key_as_string,
                // label: kata[0],
                // category2: "Batas Tanggal Lapor",
                // batasTanggalLapor: "20",
              });
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const tanggalBayar = find(data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              tanggalBayar: tanggalBayar ? tanggalBayar.tanggalBayar : "0",
              category2: "Batas Tanggal Bayar",
              batasTanggalLapor: "10",
            });
          }
        }
        return dataFinal;
      }
      if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
        const data = await elastic.getDataChartPPN(req);
        // console.log("ini cuy");
        var _data_ = [];
        var _data__ = [];
        var dataperbulan = [];
        var dataFinal = [];
        var totaldata = [];
        var totaldataPerbulan = [];
        var totaldataPertahun = [];
        var totaldataAll = [];
        // var _datasss = [];
        // var kata_ = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn:
                      dataPembetulan.sum_b1.value + dataPembetulan.sum_b2.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data_, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(_data_, {
                key: element.key,
                category: test,
              });
              totaldataPerbulan.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
                // jenisPajak: dataJenisPajak.jenisPajak,
              });
            }
          }
        }
        // return totaldataPertahun

        return req.query.rentangWaktu === "tahun"
          ? totaldataPertahun
          : totaldataPerbulan;
      }
      if (
        req.query.jenisPajak === "Jumlah PPN Terutang" ||
        req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP" ||
        req.query.jenisPajak === "Perbandingan KB/LB" ||
        req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang" ||
        req.query.jenisPajak === "Jumlah PPN terkait impor terutang"
      ) {
        const data = await elastic.getDataChartPPN(req);
        var _data_ = [];
        var _data__ = [];
        var dataperbulan = [];
        var dataFinal = [];
        var totaldata = [];
        var totaldataPerbulan = [];
        var totaldataPertahun = [];
        // var kata_ = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    // jenisPajak: dataJenisPajak.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }
        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data_, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(_data_, {
                key: element.key,
                category: test,
              });
              totaldataPerbulan.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
                // jenisPajak: dataJenisPajak.jenisPajak,
              });
            }
          }
        }
        return req.query.rentangWaktu === "tahun"
          ? totaldataPertahun
          : totaldataPerbulan;
      }
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataChartPPN(req);
        var data_ = [];
        var _data__ = [];
        var dataFinal = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataPerbulan = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataPerbulan.key_as_string,
          });
          for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
            const datanpwp = dataPerbulan.npwp.buckets[b];
            for (let c = 0; c < datanpwp.tanggal.buckets.length; c++) {
              const dataTanggal = datanpwp.tanggal.buckets[c];
              data_.push({
                category: datanpwp.key,
                key: dataPerbulan.key_as_string,
                tanggalLapor: dataTanggal.key_as_string,
                // label: kata[0],
                // category2: "Batas Tanggal Lapor",
                // batasTanggalLapor: "20",
              });
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const tanggalLapor = find(data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              tanggalLapor: tanggalLapor ? tanggalLapor.tanggalLapor : "0",
              category2: "Batas Tanggal Lapor",
              batasTanggalLapor: "20",
            });
          }
        }
        return dataFinal;
      }
    }
  })
);

router.get(
  "/summary",
  wrapAsync(async (req, res) => {
    if (
      (req.query.namaMenu === "PPH4/2" && req.query.tahunbanding) ||
      (req.query.namaMenu === "PPH23/26" && req.query.tahunbanding) ||
      (req.query.namaMenu === "PPH22" && req.query.tahunbanding)
    ) {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataSummaryPPH(req);
        var data_ = [];
        var dataSummary = [];
        var datatesting = [];
        var kata = [
          `Total Telat Lapor`,
          `Tanggal Lapor Terlama`,
          `Tanggal Lapor Tercepat`,
          `Total Belum Lapor`,
        ];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataperbulan = data.aggregations.data_perbulan.buckets[a];
          datatesting.push({
            key: dataperbulan.key_as_string,
            doc_count: dataperbulan.doc_count,
          });
          for (let b = 0; b < dataperbulan.npwp.buckets.length; b++) {
            const nilai = dataperbulan.npwp.buckets[b];
            for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
              const nilaiPembetulan = nilai.pembetulan.buckets[c];
              for (let d = 0; d < nilaiPembetulan.tanggal.buckets.length; d++) {
                const nilaitanggal = nilaiPembetulan.tanggal.buckets[d];
                data_.push({
                  nilaiTanggal: nilaitanggal.key_as_string,
                  npwp: nilai.key,
                  keyas: dataperbulan.key_as_string,
                });
              }
            }
          }
        }
        let groupKeyas = groupBy(data_, (d) => d.keyas);
        var totaldata = 0;
        var arrayNpwp = req.query.npwp.split(",");

        for (let key of Object.keys(groupKeyas)) {
          var tes = groupKeyas[key].map((a) => a.npwp);
          var tes2 = arrayNpwp.filter((b) => tes.indexOf(b) < 0);
          totaldata += tes2.length;
        }

        var maximum = Math.max.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var minimum = Math.min.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var total = data_.filter(function (o) {
          if (o.nilaiTanggal > 20) {
            return true;
          }
        });

        var totaldoc = datatesting.filter(function (o) {
          if (o.doc_count === 0) {
            return true;
          }
        });

        var totalBelumLapor = arrayNpwp.length * totaldoc.length + totaldata;
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total Telat Lapor`
                ? total.length
                : element === `Tanggal Lapor Terlama`
                ? maximum
                : element === `Total Belum Lapor`
                ? totalBelumLapor
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataChartPPH4(req);
        const data2 = await elastic.getDataChartPPH4TahunBanding(req);
        var _data = [];
        var _data_ = [];
        var _data2 = [];
        var _data2_ = [];
        var totaldata = [];
        var _data2 = [];
        var totaldata2 = [];
        var dataFinaAll = [];
        var dataSummary = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data_.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data2_.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (let a = 0; a < _data_.length; a++) {
          const element = _data_[a];
          // for (let b = 0; b < _dataJenisPajak.length; b++) {
          //   const dataJenisPajak = _dataJenisPajak[b];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data, {
              key: element.key,
              category: test,
            });
            totaldata.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            totaldata.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            totaldata.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var sum1 = _.sumBy(totaldata, function (o) {
            return o.jumlahTransaksi;
          });
        }

        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data2_, {
              key: element.key,
              category: test,
            });
            totaldata2.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
            });
          }
          var maximum2 = Math.max.apply(
            Math,
            totaldata2.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            totaldata2.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var sum2 = _.sumBy(totaldata2, function (o) {
            return o.jumlahTransaksi;
          });
        }

        dataFinaAll.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinaAll.length; b++) {
          const DataAll = dataFinaAll[b];
          for (let a = 0; a < kata.length; a++) {
            const element = kata[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      } else {
        const data = await elastic.getDataSummaryPPH(req);
        const data2 = await elastic.getDataSummaryPPHTahunBanding(req);
        var _data = [];
        var _data__ = [];
        var _data2 = [];
        var data2__ = [];
        var _dataJenisPajak = [];
        var dataFinal = [];
        var dataFinal2 = [];
        var dataSummary = [];
        var dataFinaAll = [];
        var kata = ["Total", "Terbesar", "Terkecil"];

        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  data2__.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }

        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum1 = _.sumBy(dataFinal, function (o) {
            return o.jumlahPphPpn;
          });
        }

        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(data2__, {
              key: element.key,
              category: test,
            });
            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
          var maximum2 = Math.max.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum2 = _.sumBy(dataFinal2, function (o) {
            return o.jumlahPphPpn;
          });
        }

        dataFinaAll.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinaAll.length; b++) {
          const DataAll = dataFinaAll[b];
          for (let a = 0; a < kata.length; a++) {
            const element = kata[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      }
    }
    if (
      (req.query.namaMenu === "PPH15" && req.query.tahunbanding) ||
      (req.query.namaMenu === "PPH21" && req.query.tahunbanding)
    ) {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataSummaryPPH(req);
        var data_ = [];
        var dataSummary = [];
        var datatesting = [];
        var kata = [
          `Total Telat Lapor`,
          `Tanggal Lapor Terlama`,
          `Tanggal Lapor Tercepat`,
          `Total Belum Lapor`,
        ];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataperbulan = data.aggregations.data_perbulan.buckets[a];
          datatesting.push({
            key: dataperbulan.key_as_string,
            doc_count: dataperbulan.doc_count,
          });
          for (let b = 0; b < dataperbulan.npwp.buckets.length; b++) {
            const nilai = dataperbulan.npwp.buckets[b];
            for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
              const nilaiPembetulan = nilai.pembetulan.buckets[c];
              for (let d = 0; d < nilaiPembetulan.tanggal.buckets.length; d++) {
                const nilaitanggal = nilaiPembetulan.tanggal.buckets[d];
                data_.push({
                  nilaiTanggal: nilaitanggal.key_as_string,
                  npwp: nilai.key,
                  keyas: dataperbulan.key_as_string,
                });
              }
            }
          }
        }
        let groupKeyas = groupBy(data_, (d) => d.keyas);
        var totaldata = 0;
        var arrayNpwp = req.query.npwp.split(",");

        for (let key of Object.keys(groupKeyas)) {
          var tes = groupKeyas[key].map((a) => a.npwp);
          var tes2 = arrayNpwp.filter((b) => tes.indexOf(b) < 0);
          totaldata += tes2.length;
        }

        var maximum = Math.max.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var minimum = Math.min.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var total = data_.filter(function (o) {
          if (o.nilaiTanggal > 20) {
            return true;
          }
        });

        var totaldoc = datatesting.filter(function (o) {
          if (o.doc_count === 0) {
            return true;
          }
        });

        var totalBelumLapor = arrayNpwp.length * totaldoc.length + totaldata;
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total Telat Lapor`
                ? total.length
                : element === `Tanggal Lapor Terlama`
                ? maximum
                : element === `Total Belum Lapor`
                ? totalBelumLapor
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataChartPPH4(req);
        const data2 = await elastic.getDataChartPPH4TahunBanding(req);
        var _data = [];
        var _data_ = [];
        var _data2 = [];
        var _data2_ = [];
        var totaldata = [];
        var _data2 = [];
        var totaldata2 = [];
        var dataFinaAll = [];
        var dataSummary = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data_.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[d];
            for (let c = 0; c < dataperbulan.npwp.buckets.length; c++) {
              const dataNpwp = dataperbulan.npwp.buckets[c];
              for (let b = 0; b < dataNpwp.pembetulan.buckets.length; b++) {
                const dataPembetulan = dataNpwp.pembetulan.buckets[b];
                _data2_.push({
                  category: dataNpwp.key,
                  key: datapertahun.key_as_string,
                  jumlahTransaksi: dataPembetulan.doc_count,
                  label: kata[0],
                });
              }
            }
          }
        }
        for (let a = 0; a < _data_.length; a++) {
          const element = _data_[a];
          // for (let b = 0; b < _dataJenisPajak.length; b++) {
          //   const dataJenisPajak = _dataJenisPajak[b];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data, {
              key: element.key,
              category: test,
            });
            totaldata.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            totaldata.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            totaldata.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var sum1 = _.sumBy(totaldata, function (o) {
            return o.jumlahTransaksi;
          });
        }

        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahTransaksi = find(_data2_, {
              key: element.key,
              category: test,
            });
            totaldata2.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahTransaksi
                ? jumlahTransaksi.jumlahTransaksi
                : 0,
            });
          }
          var maximum2 = Math.max.apply(
            Math,
            totaldata2.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            totaldata2.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var sum2 = _.sumBy(totaldata2, function (o) {
            return o.jumlahTransaksi;
          });
        }

        dataFinaAll.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinaAll.length; b++) {
          const DataAll = dataFinaAll[b];
          for (let a = 0; a < kata.length; a++) {
            const element = kata[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      } else {
        const data = await elastic.getDataSummaryPPH(req);
        const data2 = await elastic.getDataSummaryPPHTahunBanding(req);
        var _data = [];
        var _data__ = [];
        var _data2 = [];
        var data2__ = [];
        var _dataJenisPajak = [];
        var dataFinal = [];
        var dataFinal2 = [];
        var dataSummary = [];
        var dataFinaAll = [];
        var kata = ["Total", "Terbesar", "Terkecil"];

        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  data2__.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }

        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum1 = _.sumBy(dataFinal, function (o) {
            return o.jumlahPphPpn;
          });
        }

        for (let a = 0; a < _data2.length; a++) {
          const element = _data2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(data2__, {
              key: element.key,
              category: test,
            });
            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
          var maximum2 = Math.max.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum2 = _.sumBy(dataFinal2, function (o) {
            return o.jumlahPphPpn;
          });
        }

        dataFinaAll.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinaAll.length; b++) {
          const DataAll = dataFinaAll[b];
          for (let a = 0; a < kata.length; a++) {
            const element = kata[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      }
    }
    if (
      req.query.namaMenu === "PPH4/2" ||
      req.query.namaMenu === "PPH23/26" ||
      req.query.namaMenu === "PPH22"
    ) {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataSummaryPPH(req);
        var data_ = [];
        var dataSummary = [];
        var datatesting = [];
        var kata = [
          `Total Telat Lapor`,
          `Tanggal Lapor Terlama`,
          `Tanggal Lapor Tercepat`,
          `Total Belum Lapor`,
        ];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataperbulan = data.aggregations.data_perbulan.buckets[a];
          datatesting.push({
            key: dataperbulan.key_as_string,
            doc_count: dataperbulan.doc_count,
          });
          for (let b = 0; b < dataperbulan.npwp.buckets.length; b++) {
            const nilai = dataperbulan.npwp.buckets[b];
            for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
              const nilaiPembetulan = nilai.pembetulan.buckets[c];
              for (let d = 0; d < nilaiPembetulan.tanggal.buckets.length; d++) {
                const nilaitanggal = nilaiPembetulan.tanggal.buckets[d];
                data_.push({
                  nilaiTanggal: nilaitanggal.key_as_string,
                  npwp: nilai.key,
                  keyas: dataperbulan.key_as_string,
                });
              }
            }
          }
        }
        let groupKeyas = groupBy(data_, (d) => d.keyas);
        var totaldata = 0;
        var arrayNpwp = req.query.npwp.split(",");

        for (let key of Object.keys(groupKeyas)) {
          var tes = groupKeyas[key].map((a) => a.npwp);
          var tes2 = arrayNpwp.filter((b) => tes.indexOf(b) < 0);
          totaldata += tes2.length;
        }

        var maximum = Math.max.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var minimum = Math.min.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var total = data_.filter(function (o) {
          if (o.nilaiTanggal > 20) {
            return true;
          }
        });

        var totaldoc = datatesting.filter(function (o) {
          if (o.doc_count === 0) {
            return true;
          }
        });

        var totalBelumLapor = arrayNpwp.length * totaldoc.length + totaldata;
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total Telat Lapor`
                ? total.length
                : element === `Tanggal Lapor Terlama`
                ? maximum
                : element === `Total Belum Lapor`
                ? totalBelumLapor
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataSummaryPPH(req);
        var data_ = [];
        var dataPertahun = [];
        var dataperbulan = [];
        var totalDataPerbulan = [];
        var totalDataPertahun = [];
        var dataSummary = [];
        var totaldatas = [];
        var kata = [`Total`, `Terbesar`, `Terkecil`];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          dataPertahun.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[d];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
              const nilai = dataPerbulan.npwp.buckets[b];
              for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
                const nilaiPembetulan = nilai.pembetulan.buckets[c];
                data_.push({
                  key:
                    req.query.rentangWaktu === "tahun"
                      ? datapertahun.key_as_string
                      : dataPerbulan.key_as_string,
                  npwp: nilai.key,
                  nilaitTerbesar: nilaiPembetulan.doc_count,
                });
              }
            }
          }
        }
        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(data_, (d) => d.npwp);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.nilaitTerbesar);
            var testing = groupNpwp[key].map((a) => a.npwp);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldatas.push({
              key: bulan.shift(),
              category: testing.shift(),
              NilaiTerbesar: sum,
            });
          }
          for (let index = 0; index < dataPertahun.length; index++) {
            const element = dataPertahun[index];
            console.log(element);
            for (let test of req.query.npwp.split(",")) {
              const jumlahTransaksi = find(totaldatas, {
                key: element.key,
                category: test,
              });
              totalDataPertahun.push({
                key: element.key,
                category: test,
                jumlahTransaksi: jumlahTransaksi
                  ? jumlahTransaksi.NilaiTerbesar
                  : 0,
              });
            }
          }

          var maximum = Math.max.apply(
            Math,
            totalDataPertahun.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var minimum = Math.min.apply(
            Math,
            totalDataPertahun.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var test = _.sumBy(totalDataPertahun, function (o) {
            return o.jumlahTransaksi;
          });
        } else {
          for (let a = 0; a < dataPertahun.length; a++) {
            const element = dataPertahun[a];
            // for (let b = 0; b < _dataJenisPajak.length; b++) {
            //   const dataJenisPajak = _dataJenisPajak[b];
            console.log(element.key);
            for (let test of req.query.npwp.split(",")) {
              const jumlahTransaksi = find(data_, {
                key: element.key,
                npwp: test,
              });

              totalDataPerbulan.push({
                key: element.key,
                category: test,
                jumlahTransaksi: jumlahTransaksi
                  ? jumlahTransaksi.nilaitTerbesar
                  : 0,
                // jenisPajak: dataJenisPajak.jenisPajak,
              });
            }
            var maximum = Math.max.apply(
              Math,
              totalDataPerbulan.map(function (o) {
                return o.jumlahTransaksi;
              })
            );
            var minimum = Math.min.apply(
              Math,
              totalDataPerbulan.map(function (o) {
                return o.jumlahTransaksi;
              })
            );
            var test = _.sumBy(totalDataPerbulan, function (o) {
              return o.jumlahTransaksi;
            });
          }
        }

        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }

        return dataSummary;
      } else {
        const data = await elastic.getDataSummaryPPH(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var dataSummary = [];
        var totaldata = [];
        var totaldataPertahun = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
          var maximum = Math.max.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum = Math.min.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var test = _.sumBy(totaldataPertahun, function (o) {
            return o.jumlahPphPpn;
          });
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPPN = find(_data, {
                key: element.key,
                category: test,
              });
              dataFinal.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
              });
            }
            var maximum = Math.max.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var minimum = Math.min.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var test = _.sumBy(dataFinal, function (o) {
              return o.jumlahPphPpn;
            });
          }
        }

        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
    }
    if (req.query.namaMenu === "PPH15" || req.query.namaMenu === "PPH21") {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataSummaryPPH(req);
        var data_ = [];
        var dataSummary = [];
        var datatesting = [];
        var kata = [
          `Total Telat Lapor`,
          `Tanggal Lapor Terlama`,
          `Tanggal Lapor Tercepat`,
          `Total Belum Lapor`,
        ];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataperbulan = data.aggregations.data_perbulan.buckets[a];
          datatesting.push({
            key: dataperbulan.key_as_string,
            doc_count: dataperbulan.doc_count,
          });
          for (let b = 0; b < dataperbulan.npwp.buckets.length; b++) {
            const nilai = dataperbulan.npwp.buckets[b];
            for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
              const nilaiPembetulan = nilai.pembetulan.buckets[c];
              for (let d = 0; d < nilaiPembetulan.tanggal.buckets.length; d++) {
                const nilaitanggal = nilaiPembetulan.tanggal.buckets[d];
                data_.push({
                  nilaiTanggal: nilaitanggal.key_as_string,
                  npwp: nilai.key,
                  keyas: dataperbulan.key_as_string,
                });
              }
            }
          }
        }
        let groupKeyas = groupBy(data_, (d) => d.keyas);
        var totaldata = 0;
        var arrayNpwp = req.query.npwp.split(",");

        for (let key of Object.keys(groupKeyas)) {
          var tes = groupKeyas[key].map((a) => a.npwp);
          var tes2 = arrayNpwp.filter((b) => tes.indexOf(b) < 0);
          totaldata += tes2.length;
        }

        var maximum = Math.max.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var minimum = Math.min.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var total = data_.filter(function (o) {
          if (o.nilaiTanggal > 20) {
            return true;
          }
        });

        var totaldoc = datatesting.filter(function (o) {
          if (o.doc_count === 0) {
            return true;
          }
        });

        var totalBelumLapor = arrayNpwp.length * totaldoc.length + totaldata;
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total Telat Lapor`
                ? total.length
                : element === `Tanggal Lapor Terlama`
                ? maximum
                : element === `Total Belum Lapor`
                ? totalBelumLapor
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
      if (
        req.query.jenisPajak === "Jumlah Bukti Potong" ||
        req.query.jenisPajak === "Jumlah Setor Sendiri"
      ) {
        const data = await elastic.getDataSummaryPPH(req);
        var data_ = [];
        var dataPertahun = [];
        var dataperbulan = [];
        var totalDataPerbulan = [];
        var totalDataPertahun = [];
        var dataSummary = [];
        var totaldatas = [];
        var kata = [`Total`, `Terbesar`, `Terkecil`];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          dataPertahun.push({
            key: datapertahun.key_as_string,
          });
          for (let d = 0; d < datapertahun.data_perbulan.buckets.length; d++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[d];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.npwp.buckets.length; b++) {
              const nilai = dataPerbulan.npwp.buckets[b];
              for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
                const nilaiPembetulan = nilai.pembetulan.buckets[c];
                data_.push({
                  key:
                    req.query.rentangWaktu === "tahun"
                      ? datapertahun.key_as_string
                      : dataPerbulan.key_as_string,
                  npwp: nilai.key,
                  nilaitTerbesar: nilaiPembetulan.doc_count,
                });
              }
            }
          }
          if (req.query.rentangWaktu === "tahun") {
            let groupNpwp = groupBy(data_, (d) => d.npwp);
            for (let key of Object.keys(groupNpwp)) {
              var tes = groupNpwp[key].map((a) => a.nilaitTerbesar);
              var testing = groupNpwp[key].map((a) => a.npwp);
              var sum = tes.reduce(function (a, b) {
                return a + b;
              }, 0);
              totaldatas.push({
                key: datapertahun.key_as_string,
                category: testing.shift(),
                NilaiTerbesar: sum,
              });
            }
            for (let index = 0; index < dataPertahun.length; index++) {
              const element = dataPertahun[index];
              for (let test of req.query.npwp.split(",")) {
                const jumlahTransaksi = find(totaldatas, {
                  key: element.key,
                  category: test,
                });
                totalDataPertahun.push({
                  key: element.key,
                  category: test,
                  jumlahTransaksi: jumlahTransaksi
                    ? jumlahTransaksi.NilaiTerbesar
                    : 0,
                });
              }
            }

            var maximum = Math.max.apply(
              Math,
              totalDataPertahun.map(function (o) {
                return o.jumlahTransaksi;
              })
            );
            var minimum = Math.min.apply(
              Math,
              totalDataPertahun.map(function (o) {
                return o.jumlahTransaksi;
              })
            );
            var test = _.sumBy(totalDataPertahun, function (o) {
              return o.jumlahTransaksi;
            });
          }

          if (req.query.rentangWaktu === "periode") {
            for (let a = 0; a < dataperbulan.length; a++) {
              const element = dataperbulan[a];
              for (let test of req.query.npwp.split(",")) {
                const jumlahTransaksi = find(data_, {
                  key: element.key,
                  npwp: test,
                });
                totalDataPerbulan.push({
                  key: element.key,
                  category: test,
                  jumlahTransaksi: jumlahTransaksi
                    ? jumlahTransaksi.nilaitTerbesar
                    : 0,
                });
              }
              var maximum = Math.max.apply(
                Math,
                totalDataPerbulan.map(function (o) {
                  return o.jumlahTransaksi;
                })
              );
              var minimum = Math.min.apply(
                Math,
                totalDataPerbulan.map(function (o) {
                  return o.jumlahTransaksi;
                })
              );
              var test = _.sumBy(totalDataPerbulan, function (o) {
                return o.jumlahTransaksi;
              });
            }
          }
        }
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }

        return dataSummary;
      } else {
        const data = await elastic.getDataSummaryPPH(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var dataSummary = [];
        var totaldata = [];
        var totaldataPertahun = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let b = 0; b < datapertahun.data_perbulan.buckets.length; b++) {
            const dataperbulan = datapertahun.data_perbulan.buckets[b];
            for (let b = 0; b < dataperbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataperbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataperbulan.key_as_string,
                    // jenisPajak: dataJenisPajak.key,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata[0],
                  });
                }
              }
            }
          }
        }
        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
          var maximum = Math.max.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum = Math.min.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var test = _.sumBy(totaldataPertahun, function (o) {
            return o.jumlahPphPpn;
          });
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPPN = find(_data, {
                key: element.key,
                category: test,
              });
              dataFinal.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
              });
            }
            var maximum = Math.max.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var minimum = Math.min.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var test = _.sumBy(dataFinal, function (o) {
              return o.jumlahPphPpn;
            });
          }
        }

        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
    }
    if (req.query.namaMenu === "EBilling" && req.query.tahunbanding) {
      if (
        req.query.jenisPajak === `Jumlah ssp generate billing` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui BNIDirect` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui MPN Pajakku` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui CMS BRI` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui MCM Mandiri`
      ) {
        const data = await elastic.getDataSummary(req);
        const data2 = await elastic.getDataSummaryTahunBanding(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var _dataEbilling2 = [];
        var __dataEbilling2__ = [];
        var dataFinaEbilling = [];
        var dataFinaAll = [];
        var dataSummary = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataTotalCount = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataTotalCount.key_as_string,
          });
          for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
            const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
            for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
              const dataNpwp = dataJenisPajak.npwp.buckets[c];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                jumlahSsp: dataNpwp.sum_data.value,
              });
            }
          }
        }

        for (
          let a = 0;
          a < data2.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataTotalCount = data2.aggregations.data_perbulan.buckets[a];
          __dataEbilling2__.push({
            key: dataTotalCount.key_as_string,
          });
          for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
            const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
            for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
              const dataNpwp = dataJenisPajak.npwp.buckets[c];
              _dataEbilling2.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                jumlahSsp: dataNpwp.sum_data.value,
              });
            }
          }
        }

        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahEbilling = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahSsp: jumlahEbilling ? jumlahEbilling.jumlahSsp : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahSsp;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahSsp;
            })
          );
          var sum1 = _.sumBy(dataFinal, function (o) {
            return o.jumlahSsp;
          });
        }

        for (let a = 0; a < __dataEbilling2__.length; a++) {
          const element = __dataEbilling2__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahEbilling = find(_dataEbilling2, {
              key: element.key,
              category: test,
            });
            dataFinaEbilling.push({
              key: element.key,
              category: test,
              jumlahSsp: jumlahEbilling ? jumlahEbilling.jumlahSsp : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }

          var maximum2 = Math.max.apply(
            Math,
            dataFinaEbilling.map(function (o) {
              return o.jumlahSsp;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            dataFinaEbilling.map(function (o) {
              return o.jumlahSsp;
            })
          );
          var sum2 = _.sumBy(dataFinaEbilling, function (o) {
            return o.jumlahSsp;
          });
        }

        dataFinaAll.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinaAll.length; b++) {
          const DataAll = dataFinaAll[b];
          for (let a = 0; a < kata.length; a++) {
            const element = kata[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      }
      if (
        req.query.jenisPajak ===
          `Jumlah pembayaran melalui BNIDirect (Rupiah)` ||
        req.query.jenisPajak === `Jumlah pembayaran melalui CMS BRI (Rupiah)` ||
        req.query.jenisPajak ===
          `Jumlah pembayaran melalui MPN Pajakku (Rupiah)` ||
        req.query.jenisPajak ===
          `Jumlah pembayaran melalui MCM Mandiri (Rupiah)` ||
        req.query.jenisPajak === `Jumlah amount generate billing (Rupiah)`
      ) {
        const data = await elastic.getDataSummary(req);
        const data2 = await elastic.getDataSummaryTahunBanding(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var _dataEbilling2 = [];
        var __dataEbilling2__ = [];
        var dataFinaEbilling = [];
        var dataFinaAll = [];
        var dataSummary = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataTotalCount = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataTotalCount.key_as_string,
          });
          for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
            const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
            for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
              const dataNpwp = dataJenisPajak.npwp.buckets[c];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                jumlahTransaksi: dataNpwp.sum_data.value,
              });
            }
          }
        }

        for (
          let a = 0;
          a < data2.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataTotalCount = data2.aggregations.data_perbulan.buckets[a];
          __dataEbilling2__.push({
            key: dataTotalCount.key_as_string,
          });
          for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
            const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
            for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
              const dataNpwp = dataJenisPajak.npwp.buckets[c];
              _dataEbilling2.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                jumlahTransaksi: dataNpwp.sum_data.value,
              });
            }
          }
        }

        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahEbilling = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahEbilling
                ? jumlahEbilling.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var sum1 = _.sumBy(dataFinal, function (o) {
            return o.jumlahTransaksi;
          });
        }

        for (let a = 0; a < __dataEbilling2__.length; a++) {
          const element = __dataEbilling2__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahEbilling = find(_dataEbilling2, {
              key: element.key,
              category: test,
            });
            dataFinaEbilling.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahEbilling
                ? jumlahEbilling.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }

          var maximum2 = Math.max.apply(
            Math,
            dataFinaEbilling.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            dataFinaEbilling.map(function (o) {
              return o.jumlahTransaksi;
            })
          );
          var sum2 = _.sumBy(dataFinaEbilling, function (o) {
            return o.jumlahTransaksi;
          });
        }

        dataFinaAll.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinaAll.length; b++) {
          const DataAll = dataFinaAll[b];
          for (let a = 0; a < kata.length; a++) {
            const element = kata[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      }
    }
    if (req.query.namaMenu === "EBilling") {
      if (
        req.query.jenisPajak === `Jumlah ssp generate billing` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui BNIDirect` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui MPN Pajakku` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui CMS BRI` ||
        req.query.jenisPajak === `Jumlah SSP pembayaran melalui MCM Mandiri`
      ) {
        const data = await elastic.getDataSummary(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var dataSummary = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataTotalCount = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataTotalCount.key_as_string,
          });
          for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
            const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
            for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
              const dataNpwp = dataJenisPajak.npwp.buckets[c];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                jumlahSsp: dataNpwp.sum_data.value,
              });
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahEbilling = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahSsp: jumlahEbilling ? jumlahEbilling.jumlahSsp : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          if (req.query.rentangWaktu === "tahun") {
            let groupNpwp = groupBy(dataFinal, (d) => d.category);
            var totaldata = [];
            for (let key of Object.keys(groupNpwp)) {
              var tes = groupNpwp[key].map((a) => a.jumlahSsp);
              var sum = tes.reduce(function (a, b) {
                return a + b;
              }, 0);
              totaldata.push({
                NilaiTerbesar: sum,
              });
            }
            var maximum = Math.max.apply(
              Math,
              totaldata.map(function (o) {
                return o.NilaiTerbesar;
              })
            );
            var minimum = Math.min.apply(
              Math,
              totaldata.map(function (o) {
                return o.NilaiTerbesar;
              })
            );
            var test = _.sumBy(totaldata, function (o) {
              return o.NilaiTerbesar;
            });
          } else {
            var maximum = Math.max.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahSsp;
              })
            );
            var minimum = Math.min.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahSsp;
              })
            );
            var test = _.sumBy(dataFinal, function (o) {
              return o.jumlahSsp;
            });
          }
        }
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }

        return dataSummary;
      }
      if (
        req.query.jenisPajak ===
          `Jumlah pembayaran melalui BNIDirect (Rupiah)` ||
        req.query.jenisPajak === `Jumlah pembayaran melalui CMS BRI (Rupiah)` ||
        req.query.jenisPajak ===
          `Jumlah pembayaran melalui MPN Pajakku (Rupiah)` ||
        req.query.jenisPajak ===
          `Jumlah pembayaran melalui MCM Mandiri (Rupiah)` ||
        req.query.jenisPajak === `Jumlah amount generate billing (Rupiah)`
      ) {
        const data = await elastic.getDataSummary(req);
        var _data = [];
        var _data__ = [];
        var dataFinal = [];
        var dataSummary = [];
        var kata = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataTotalCount = data.aggregations.data_perbulan.buckets[a];
          _data__.push({
            key: dataTotalCount.key_as_string,
          });
          for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
            const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
            for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
              const dataNpwp = dataJenisPajak.npwp.buckets[c];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                jumlahTransaksi: dataNpwp.sum_data.value,
              });
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahEbilling = find(_data, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahTransaksi: jumlahEbilling
                ? jumlahEbilling.jumlahTransaksi
                : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          if (req.query.rentangWaktu === "tahun") {
            let groupNpwp = groupBy(dataFinal, (d) => d.category);
            var totaldata = [];
            for (let key of Object.keys(groupNpwp)) {
              var tes = groupNpwp[key].map((a) => a.jumlahTransaksi);
              var sum = tes.reduce(function (a, b) {
                return a + b;
              }, 0);
              totaldata.push({
                NilaiTerbesar: sum,
              });
            }
            var maximum = Math.max.apply(
              Math,
              totaldata.map(function (o) {
                return o.NilaiTerbesar;
              })
            );
            var minimum = Math.min.apply(
              Math,
              totaldata.map(function (o) {
                return o.NilaiTerbesar;
              })
            );
            var test = _.sumBy(totaldata, function (o) {
              return o.NilaiTerbesar;
            });
          } else {
            var maximum = Math.max.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahTransaksi;
              })
            );
            var minimum = Math.min.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahTransaksi;
              })
            );
            var test = _.sumBy(dataFinal, function (o) {
              return o.jumlahTransaksi;
            });
          }
        }
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }

        return dataSummary;
      }
      // else {
      //   const data = await elastic.getDataSummary(req);
      //   var data_ = [];
      //   var kata = [`Total`, `Terbesar`, `Terkecil`];
      //   for (var i = 0; i < kata.length; i++) {
      //     const element = kata[i];
      //     data_.push({
      //       value:
      //         element === `Total`
      //           ? data.aggregations.sum.value
      //           : element === `Terbesar`
      //           ? data.aggregations.max.value
      //           : data.aggregations.min.value,
      //       label: element,
      //     });
      //   }
      //   return data_;
      // }
    }
    if (req.query.namaMenu === "BKWP" && req.query.tahunbanding) {
      const data = await elastic.getDataSummary(req);
      const data2 = await elastic.getDataSummaryTahunBanding(req);
      var _data = [];
      var _data__ = [];
      var _data2 = [];
      var data2_ = [];
      var dataSummary = [];
      var dataFinal = [];
      var dataFinalbwkp2 = [];
      var dataFinaAll = [];
      var katabkwp = ["Total", "Terbesar", "Terkecil"];
      for (let a = 0; a < data.aggregations.data_perbulan.buckets.length; a++) {
        const dataTotalCount = data.aggregations.data_perbulan.buckets[a];
        _data__.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwpCompany.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwpCompany.buckets[c];
            for (let d = 0; d < dataNpwp.status.buckets.length; d++) {
              const dataStatus = dataNpwp.status.buckets[d];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                // jenisPajak: dataJenisPajak.key,
                jumlah: dataStatus.sum_data.value,
                // label: kata[0],
              });
            }
          }
        }
      }
      for (
        let a = 0;
        a < data2.aggregations.data_perbulan.buckets.length;
        a++
      ) {
        const dataTotalCount = data2.aggregations.data_perbulan.buckets[a];
        _data2.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwpCompany.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwpCompany.buckets[c];
            for (let d = 0; d < dataNpwp.status.buckets.length; d++) {
              const dataStatus = dataNpwp.status.buckets[d];
              data2_.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                // jenisPajak: dataJenisPajak.key,
                jumlah: dataStatus.sum_data.value,
                // label: kata[0],
              });
            }
          }
        }
      }

      for (let a = 0; a < _data__.length; a++) {
        const element = _data__[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahBkwp = find(_data, {
            key: element.key,
            category: test,
          });
          dataFinal.push({
            key: element.key,
            category: test,
            jumlah: jumlahBkwp ? jumlahBkwp.jumlah : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }

        var maximum1 = Math.max.apply(
          Math,
          dataFinal.map(function (o) {
            return o.jumlah;
          })
        );
        var minimum1 = Math.min.apply(
          Math,
          dataFinal.map(function (o) {
            return o.jumlah;
          })
        );
        var sum1 = _.sumBy(dataFinal, function (o) {
          return o.jumlah;
        });
      }

      for (let a = 0; a < _data2.length; a++) {
        const element = _data2[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahBkwp = find(data2_, {
            key: element.key,
            category: test,
          });
          dataFinal.push({
            key: element.key,
            category: test,
            jumlah: jumlahBkwp ? jumlahBkwp.jumlah : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
        var maximum2 = Math.max.apply(
          Math,
          dataFinalbwkp2.map(function (o) {
            return o.jumlah;
          })
        );
        var minimum2 = Math.min.apply(
          Math,
          dataFinalbwkp2.map(function (o) {
            return o.jumlah;
          })
        );
        var sum2 = _.sumBy(dataFinalbwkp2, function (o) {
          return o.jumlah;
        });
      }

      dataFinaAll.push({
        terbesar: maximum1,
        terbesarCompare: maximum2,
        trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
        terkecil: minimum1,
        terkecilCompare: minimum2,
        trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
        sum: sum1,
        sumCompare: sum2,
        trendSum: ((sum1 - sum2) / sum2) * 100,
      });

      for (let b = 0; b < dataFinaAll.length; b++) {
        const DataAll = dataFinaAll[b];
        for (let a = 0; a < katabkwp.length; a++) {
          const element = katabkwp[a];
          dataSummary.push({
            value:
              element === `Total`
                ? DataAll.sum
                : element === `Terbesar`
                ? DataAll.terbesar
                : DataAll.terkecil,
            label: element,
            trend:
              element === `Total`
                ? DataAll.trendSum
                : element === `Terbesar`
                ? DataAll.trendTerbesar
                : DataAll.trendTerkecil,
            lastValue:
              element === `Total`
                ? DataAll.sumCompare
                : element === `Terbesar`
                ? DataAll.terbesarCompare
                : DataAll.terkecilCompare,
          });
        }
      }

      return dataSummary;
    }
    if (req.query.namaMenu === "BKWP") {
      const data = await elastic.getDataSummary(req);
      var _data = [];
      var _data__ = [];
      var dataSummary = [];
      var dataFinal = [];
      var kata = ["Total", "Terbesar", "Terkecil"];
      for (let a = 0; a < data.aggregations.data_perbulan.buckets.length; a++) {
        const dataTotalCount = data.aggregations.data_perbulan.buckets[a];
        _data__.push({
          key: dataTotalCount.key_as_string,
        });
        for (let b = 0; b < dataTotalCount.jenisPajak.buckets.length; b++) {
          const dataJenisPajak = dataTotalCount.jenisPajak.buckets[b];
          for (let c = 0; c < dataJenisPajak.npwpCompany.buckets.length; c++) {
            const dataNpwp = dataJenisPajak.npwpCompany.buckets[c];
            for (let d = 0; d < dataNpwp.status.buckets.length; d++) {
              const dataStatus = dataNpwp.status.buckets[d];
              _data.push({
                category: dataNpwp.key,
                key: dataTotalCount.key_as_string,
                // jenisPajak: dataJenisPajak.key,
                jumlah: dataStatus.sum_data.value,
                // label: kata[0],
              });
            }
          }
        }
      }
      for (let a = 0; a < _data__.length; a++) {
        const element = _data__[a];
        for (let test of req.query.npwp.split(",")) {
          const jumlahBkwp = find(_data, {
            key: element.key,
            category: test,
          });
          dataFinal.push({
            key: element.key,
            category: test,
            jumlah: jumlahBkwp ? jumlahBkwp.jumlah : 0,
            // jenisPajak: dataJenisPajak.jenisPajak,
          });
        }
        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(dataFinal, (d) => d.category);
          var totaldata = [];
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlah);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              NilaiTerbesar: sum,
            });
          }
          var maximum = Math.max.apply(
            Math,
            totaldata.map(function (o) {
              return o.NilaiTerbesar;
            })
          );
          var minimum = Math.min.apply(
            Math,
            totaldata.map(function (o) {
              return o.NilaiTerbesar;
            })
          );
          var test = _.sumBy(totaldata, function (o) {
            return o.NilaiTerbesar;
          });
        } else {
          var maximum = Math.max.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlah;
            })
          );
          var minimum = Math.min.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlah;
            })
          );
          var test = _.sumBy(dataFinal, function (o) {
            return o.jumlah;
          });
        }
      }
      for (let a = 0; a < kata.length; a++) {
        const element = kata[a];
        dataSummary.push({
          value:
            element === `Total`
              ? test
              : element === `Terbesar`
              ? maximum
              : minimum,
          label: element,
        });
      }

      return dataSummary;
    }
    if (req.query.namaMenu === "PPN" && req.query.tahunbanding) {
      if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
        const data = await elastic.getDataSummaryPPN(req);
        const data2 = await elastic.getDataSummaryPPNTahunBanding(req);
        var _data_ = [];
        var dataPertahun = [];
        var dataperbulan = [];
        var datapertahun2 = [];
        var dataperbulan2 = [];
        var _data_ = [];
        var _data_ = [];
        var _data2_ = [];
        var _data__ = [];
        var _data2__ = [];
        var dataFinal = [];
        var dataFinal2 = [];
        var dataFinalall = [];
        var dataSummary = [];
        // var _datasss = [];
        var kata_ = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn:
                      dataPembetulan.sum_b1.value + dataPembetulan.sum_b2.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          datapertahun2.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan2.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data2_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn:
                      dataPembetulan.sum_b1.value + dataPembetulan.sum_b2.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPphPpn = find(_data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum1 = _.sumBy(dataFinal, function (o) {
            return o.jumlahPphPpn;
          });
        }
        for (let a = 0; a < datapertahun2.length; a++) {
          const element = datapertahun2[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPphPpn = find(_data2_, {
              key: element.key,
              category: test,
            });

            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              // jenisPajak: dataJenisPajak.jenisPajak,
            });
          }
          var maximum2 = Math.max.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum2 = _.sumBy(dataFinal2, function (o) {
            return o.jumlahPphPpn;
          });
        }

        dataFinalall.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinalall.length; b++) {
          const DataAll = dataFinalall[b];
          for (let a = 0; a < kata_.length; a++) {
            const element = kata_[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      }
      if (
        req.query.jenisPajak === "Jumlah PPN Terutang" ||
        req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP" ||
        req.query.jenisPajak === "Perbandingan KB/LB" ||
        req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang" ||
        req.query.jenisPajak === "Jumlah PPN terkait impor terutang"
      ) {
        const data = await elastic.getDataSummaryPPN(req);
        const data2 = await elastic.getDataSummaryPPNTahunBanding(req);
        var _data_ = [];
        var dataFinal = [];
        var _data2_ = [];
        var dataFinal2 = [];
        var dataFinalall = [];
        // var _datasss = [];
        var dataSummary = [];
        var _data__ = [];
        var _data2__ = [];
        var dataperbulan = [];
        var dataperbulan2 = [];
        var kata_ = ["Total", "Terbesar", "Terkecil"];
        //Data 1
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    // jenisPajak: dataJenisPajak.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }
        for (let a = 0; a < _data__.length; a++) {
          const element = _data__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data_, {
              key: element.key,
              category: test,
            });
            dataFinal.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
          var maximum1 = Math.max.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum1 = Math.min.apply(
            Math,
            dataFinal.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum1 = _.sumBy(dataFinal, function (o) {
            return o.jumlahPphPpn;
          });
        }
        //Data 2
        for (
          let a = 0;
          a < data2.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data2.aggregations.data_pertahun.buckets[a];
          _data2__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan2.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data2_.push({
                    category: dataNpwp.key,
                    // jenisPajak: dataJenisPajak.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        for (let a = 0; a < _data2__.length; a++) {
          const element = _data2__[a];
          for (let test of req.query.npwp.split(",")) {
            const jumlahPPN = find(_data2_, {
              key: element.key,
              category: test,
            });
            dataFinal2.push({
              key: element.key,
              category: test,
              jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
            });
          }
          var maximum2 = Math.max.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum2 = Math.min.apply(
            Math,
            dataFinal2.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var sum2 = _.sumBy(dataFinal2, function (o) {
            return o.jumlahPphPpn;
          });
        }

        dataFinalall.push({
          terbesar: maximum1,
          terbesarCompare: maximum2,
          trendTerbesar: ((maximum1 - maximum2) / maximum2) * 100,
          terkecil: minimum1,
          terkecilCompare: minimum2,
          trendTerkecil: ((minimum1 - minimum2) / minimum2) * 100,
          sum: sum1,
          sumCompare: sum2,
          trendSum: ((sum1 - sum2) / sum2) * 100,
        });

        for (let b = 0; b < dataFinalall.length; b++) {
          const DataAll = dataFinalall[b];
          for (let a = 0; a < kata_.length; a++) {
            const element = kata_[a];
            dataSummary.push({
              value:
                element === `Total`
                  ? DataAll.sum
                  : element === `Terbesar`
                  ? DataAll.terbesar
                  : DataAll.terkecil,
              label: element,
              trend:
                element === `Total`
                  ? DataAll.trendSum
                  : element === `Terbesar`
                  ? DataAll.trendTerbesar
                  : DataAll.trendTerkecil,
              lastValue:
                element === `Total`
                  ? DataAll.sumCompare
                  : element === `Terbesar`
                  ? DataAll.terbesarCompare
                  : DataAll.terkecilCompare,
            });
          }
        }

        return dataSummary;
      }
    }
    if (req.query.namaMenu === "PPN") {
      if (req.query.jenisPajak === "Tanggal Lapor") {
        const data = await elastic.getDataSummaryPPN(req);
        var data_ = [];
        var dataSummary = [];
        var datatesting = [];
        var kata = [
          `Total Telat Lapor`,
          `Tanggal Lapor Terlama`,
          `Tanggal Lapor Tercepat`,
          `Total Belum Lapor`,
        ];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataperbulan = data.aggregations.data_perbulan.buckets[a];
          datatesting.push({
            key: dataperbulan.key_as_string,
            doc_count: dataperbulan.doc_count,
          });
          for (let b = 0; b < dataperbulan.npwp.buckets.length; b++) {
            const nilai = dataperbulan.npwp.buckets[b];
            for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
              const nilaiPembetulan = nilai.pembetulan.buckets[c];
              for (let d = 0; d < nilaiPembetulan.tanggal.buckets.length; d++) {
                const nilaitanggal = nilaiPembetulan.tanggal.buckets[d];
                data_.push({
                  nilaiTanggal: nilaitanggal.key_as_string,
                  npwp: nilai.key,
                  keyas: dataperbulan.key_as_string,
                });
              }
            }
          }
        }
        let groupKeyas = groupBy(data_, (d) => d.keyas);
        var totaldata = 0;
        var arrayNpwp = req.query.npwp.split(",");

        for (let key of Object.keys(groupKeyas)) {
          var tes = groupKeyas[key].map((a) => a.npwp);
          var tes2 = arrayNpwp.filter((b) => tes.indexOf(b) < 0);
          totaldata += tes2.length;
        }

        var maximum = Math.max.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var minimum = Math.min.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var total = data_.filter(function (o) {
          if (o.nilaiTanggal > 20) {
            return true;
          }
        });

        var totaldoc = datatesting.filter(function (o) {
          if (o.doc_count === 0) {
            return true;
          }
        });

        var totalBelumLapor = arrayNpwp.length * totaldoc.length + totaldata;
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total Telat Lapor`
                ? total.length
                : element === `Tanggal Lapor Terlama`
                ? maximum
                : element === `Total Belum Lapor`
                ? totalBelumLapor
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
      if (req.query.jenisPajak === "Tanggal Bayar") {
        const data = await elastic.getDataSummaryPPN(req);
        var data_ = [];
        var dataSummary = [];
        var datatesting = [];
        var kata = [
          `Total Telat Bayar`,
          `Tanggal Bayar Terlama`,
          `Tanggal Bayar Tercepat`,
        ];
        for (
          let a = 0;
          a < data.aggregations.data_perbulan.buckets.length;
          a++
        ) {
          const dataperbulan = data.aggregations.data_perbulan.buckets[a];
          datatesting.push({
            key: dataperbulan.key_as_string,
            doc_count: dataperbulan.doc_count,
          });
          for (let b = 0; b < dataperbulan.npwp.buckets.length; b++) {
            const nilai = dataperbulan.npwp.buckets[b];
            for (let c = 0; c < nilai.pembetulan.buckets.length; c++) {
              const nilaiPembetulan = nilai.pembetulan.buckets[c];
              for (let d = 0; d < nilaiPembetulan.tanggal.buckets.length; d++) {
                const nilaitanggal = nilaiPembetulan.tanggal.buckets[d];
                data_.push({
                  nilaiTanggal: nilaitanggal.key_as_string,
                  npwp: nilai.key,
                  keyas: dataperbulan.key_as_string,
                });
              }
            }
          }
        }
        // let groupKeyas = groupBy(data_, (d) => d.keyas);
        // var totaldata = 0;
        // var arrayNpwp = req.query.npwp.split(",");

        // for (let key of Object.keys(groupKeyas)) {
        //   var tes = groupKeyas[key].map((a) => a.npwp);
        //   var tes2 = arrayNpwp.filter((b) => tes.indexOf(b) < 0);
        //   totaldata += tes2.length;
        // }

        var maximum = Math.max.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var minimum = Math.min.apply(
          Math,
          data_.map(function (o) {
            return o.nilaiTanggal;
          })
        );
        var total = data_.filter(function (o) {
          if (o.nilaiTanggal > 10) {
            return true;
          }
        });

        // var totaldoc = datatesting.filter(function (o) {
        //   if (o.doc_count === 0) {
        //     return true;
        //   }
        // });

        // var totalBelumLapor = arrayNpwp.length * totaldoc.length + totaldata;
        for (let a = 0; a < kata.length; a++) {
          const element = kata[a];
          dataSummary.push({
            value:
              element === `Total Telat Bayar`
                ? total.length
                : element === `Tanggal Bayar Terlama`
                ? maximum
                : minimum,
            label: element,
          });
        }
        return dataSummary;
      }
      if (req.query.jenisPajak === "Jumlah Kompensasi PPN") {
        const data = await elastic.getDataSummaryPPN(req);
        var _data_ = [];
        var _data__ = [];
        var dataperbulan = [];
        var dataFinal = [];
        var totaldata = [];
        var dataSummary = [];
        var totaldataPertahun = [];
        var kata_ = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn:
                      dataPembetulan.sum_b1.value + dataPembetulan.sum_b2.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data_, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
          var maximum = Math.max.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum = Math.min.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var test = _.sumBy(totaldataPertahun, function (o) {
            return o.jumlahPphPpn;
          });
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPPN = find(_data_, {
                key: element.key,
                category: test,
              });
              dataFinal.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
              });
            }
            var maximum = Math.max.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var minimum = Math.min.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var test = _.sumBy(dataFinal, function (o) {
              return o.jumlahPphPpn;
            });
          }
        }
        for (let a = 0; a < kata_.length; a++) {
          const element = kata_[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }

        return dataSummary;
      }
      if (
        req.query.jenisPajak === "Jumlah PPN Terutang" ||
        req.query.jenisPajak === "Jumlah DPP terkait ekspor terutang" ||
        req.query.jenisPajak === "Jumlah PPN terutang per Kode MAP" ||
        req.query.jenisPajak === "Perbandingan KB/LB" ||
        req.query.jenisPajak === "Jumlah PPN terkait impor terutang"
      ) {
        const data = await elastic.getDataSummaryPPN(req);
        var _data_ = [];
        var _data__ = [];
        var dataperbulan = [];
        var dataFinal = [];
        var totaldata = [];
        var dataSummary = [];
        var totaldataPertahun = [];
        var kata_ = ["Total", "Terbesar", "Terkecil"];
        for (
          let a = 0;
          a < data.aggregations.data_pertahun.buckets.length;
          a++
        ) {
          const datapertahun = data.aggregations.data_pertahun.buckets[a];
          _data__.push({
            key: datapertahun.key_as_string,
          });
          for (let x = 0; x < datapertahun.data_perbulan.buckets.length; x++) {
            const dataPerbulan = datapertahun.data_perbulan.buckets[x];
            dataperbulan.push({
              key: dataPerbulan.key_as_string,
            });
            for (let b = 0; b < dataPerbulan.jenisPajak.buckets.length; b++) {
              const dataJenisPajak = dataPerbulan.jenisPajak.buckets[b];
              for (let c = 0; c < dataJenisPajak.npwp.buckets.length; c++) {
                const dataNpwp = dataJenisPajak.npwp.buckets[c];
                for (let d = 0; d < dataNpwp.pembetulan.buckets.length; d++) {
                  const dataPembetulan = dataNpwp.pembetulan.buckets[d];
                  _data_.push({
                    category: dataNpwp.key,
                    key:
                      req.query.rentangWaktu === "tahun"
                        ? datapertahun.key_as_string
                        : dataPerbulan.key_as_string,
                    jumlahPphPpn: dataPembetulan.jumlah.value,
                    // label: kata_[0],
                  });
                }
              }
            }
          }
        }

        if (req.query.rentangWaktu === "tahun") {
          let groupNpwp = groupBy(_data_, (d) => d.category);
          for (let key of Object.keys(groupNpwp)) {
            var tes = groupNpwp[key].map((a) => a.jumlahPphPpn);
            var testing = groupNpwp[key].map((a) => a.category);
            var bulan = groupNpwp[key].map((a) => a.key);
            var sum = tes.reduce(function (a, b) {
              return a + b;
            }, 0);
            totaldata.push({
              category: testing.shift(),
              key: bulan.shift(),
              jumlahPphPpn: sum,
              // label: kata[0],
            });
          }
          for (let index = 0; index < _data__.length; index++) {
            const element = _data__[index];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPphPpn = find(totaldata, {
                key: element.key,
                category: test,
              });
              totaldataPertahun.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPphPpn ? jumlahPphPpn.jumlahPphPpn : 0,
              });
            }
          }
          var maximum = Math.max.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var minimum = Math.min.apply(
            Math,
            totaldataPertahun.map(function (o) {
              return o.jumlahPphPpn;
            })
          );
          var test = _.sumBy(totaldataPertahun, function (o) {
            return o.jumlahPphPpn;
          });
        } else {
          for (let a = 0; a < _data__.length; a++) {
            const element = _data__[a];
            for (let test of req.query.npwp.split(",")) {
              const jumlahPPN = find(_data_, {
                key: element.key,
                category: test,
              });
              dataFinal.push({
                key: element.key,
                category: test,
                jumlahPphPpn: jumlahPPN ? jumlahPPN.jumlahPphPpn : 0,
              });
            }
            var maximum = Math.max.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var minimum = Math.min.apply(
              Math,
              dataFinal.map(function (o) {
                return o.jumlahPphPpn;
              })
            );
            var test = _.sumBy(dataFinal, function (o) {
              return o.jumlahPphPpn;
            });
          }
        }
        for (let a = 0; a < kata_.length; a++) {
          const element = kata_[a];
          dataSummary.push({
            value:
              element === `Total`
                ? test
                : element === `Terbesar`
                ? maximum
                : minimum,
            label: element,
          });
        }

        return dataSummary;
      }
    }
  })
);

router.get("/mongodb", mongodb.mongodb.kategori);
router.get("/mongodbAlias", auth.decodeProfile(), mongodb.mongodb.alias);
router.get("/mongodbAlias/:npwp", mongodb.mongodb.aliasByNpwp);
router.get("/menu", mongodb.mongodb.getForMenukategori);
router.get("/menuMap", mongodb.mongodb.getForMenuMap);
router.post("/mongodbNpwp", auth.decodeProfile('post'), mongodb.createDataNpwp);
router.post("/mongodb", mongodb.createData);
router.delete("/mongodb/:id", mongodb.deleteKategori);
router.put("/mongodb/:id", mongodb.update);

module.exports = router;
