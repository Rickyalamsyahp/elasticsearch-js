import { version } from "../../package.json";
import { Router } from "express";
import sso from "./sso";
import env from "./env";
import _private from "./private";
const bodyParser = require("body-parser");
const router = require("./elastic/index");

export default (app) => {
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.use("/api/sso", sso);
  app.use("/api/env", env);
  app.use("/api/private", _private);
  app.use("/api", router);

  // perhaps expose some API metadata at the root
  app.use("/api/version", (req, res) => {
    res.json({ version });
  });
};
