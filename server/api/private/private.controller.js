import axios from 'axios';
import config from '../../../config/environment';
import queryString from 'query-string';
import request from 'request-promise';
import fs from 'fs';
import cache from 'memory-cache';
import moment from 'moment';
import jwt from 'jsonwebtoken';
import compose from 'composable-middleware';
import _ from 'lodash';

export const createPajakkuProfile = (req) => {
  let userData = req.cookies['USER-PROFILE'] || req.headers['x-user-profile'];
  if(userData){
    let user = JSON.parse(userData);
    user.id = String(user.id);
    
    let jwtData = {
      iss: 'MPN Widget',
      aud: ['Ebilling API Internal'],
      jti: 'def826b5-43df-4d76-8a0c-fb23dbf33e23',
      exp: moment().add(1,'day').valueOf(),
      iat: new Date().valueOf(),
      sub: user.username,
      metadata: user
    };

    // let sign = jwt.sign(jwtData, 'sharedKey');
    let sign = jwt.sign(jwtData, config.private.credentials.client_secret);
    req.headers['x-profile'] = sign;
  }
}

export const index = () => {
  return compose()
  .use(async (req, res, next) => {

    let resource = req.params.resource;
    req.headers['content-type'] = 'application/json';
    req.headers['x-no-compression'] = true;
    req.headers['Host'] = config.private.hostName;

    req.headers['x-client'] = config.private.credentials.client_id;
    createPajakkuProfile(req)
    
    if(req.query && Object.keys(req.query).length > 0){
      resource += `?${queryString.stringify(req.query)}`;
    }

    let url = `${config.private.host}${config.private.baseUrl}${resource}`;
    
    try {
      const method = req.method.toLowerCase();
      let result = method === 'get' || method === 'delete' ? 
        await axios[method](url,{headers:req.headers}) : 
        await axios[method](url,req.body,{headers:req.headers});

      // if(result.headers) res.set(result.headers);
      return res.status(result.status || 200).json(result.data);
    } catch (err){
      return errorHandler(res, err);
    }
  })
} 

export const upload = () => {
  return compose()
  .use(async (req, res, next) => {
    let resource = req.params.resource;
    let url = `${config.private.host}${config.private.baseUrl}${resource}`;
    
    let formData = {
      file: fs.createReadStream(req.file.path),
      originalname: req.file.originalname,
      mimetype: req.file.mimetype,
      size: req.file.size
    }

    for(let key of Object.keys(req.body)){
      formData[key] = req.body[key]
    }
    
    try{
      const pajakkuProfile = createPajakkuProfile(req)
      const opts = {
        uri:url,
        method:'POST',
        headers:{
          authorization:req.headers.authorization,
          'x-profile':pajakkuProfile,
          'x-client': config.private.credentials.client_id,
        },
        formData
      }


      const result = await request(opts);
      return res.status(200).send(result);
    } catch (err){
      return errorHandler(res, err)
    }
  })
}

const errorHandler = (res, err) => {
  if(err.response.status === 401) cache.clear();
  console.log(err.response.data);
  const keys = Object.keys(err.response.headers);
  for(let key of keys){
    if(['server', 'content-type', 'date', 'transfer-encoding', 'connection', 'vary'].indexOf(key) < 0)
      res.set(key, err.response.headers[key]);
    // res.set(err.response.headers);
  }
  return res.status(err.response.status).json(err.response.data ? err.response.data : {errorMessage: 'Something Wrong'})
}