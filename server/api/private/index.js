import express from 'express';
import * as auth from '../../services/auth.service';
import controller from './private.controller';
import multer from 'multer'
import config from '../../../config/environment'

module.exports = target => {
  const router = express.Router();
  router.get(`/:resource`, auth.isAuthenticated('private'), controller.index(target));
  router.post(`/:resource`, auth.isAuthenticated('private'), controller.index(target));
  router.delete(`/:resource`, auth.isAuthenticated('private'), controller.index(target));
  router.put(`/:resource`, auth.isAuthenticated('private'), controller.index(target));

  router.post(`/upload/:resource`, multer({dest: config.path.upload}).single('file'), auth.isAuthenticated('private'), controller.upload(target));
  router.put(`/upload/:resource`, multer({dest: config.path.upload}).single('file'), auth.isAuthenticated('private'), controller.upload(target));
  return router;
}