import config from '../../../config/environment';
import pjson from '../../../package.json';
import md5 from 'md5';
import { cloneDeep } from 'lodash';

export const index = (req,res) => {
  let { sso, apiGateway, theme, appConsole, widgetInterface, widgetGateway, kbs, applicationType, locale, portal  } = config;
  let appInfo = {
    name:pjson.name,
    version:pjson.version,
    appname: pjson.appname && pjson.appname !== '-' ? pjson.appname : null,
    subname:pjson.subname && pjson.subname !== '-' ? pjson.subname : null,
  }
  apiGateway = cloneDeep(apiGateway);
  delete apiGateway.clientSecret;

  return res.status(200).json({
    appInfo,
    timestamp:new Date(),
    theme: md5(theme),
    apiGateway,
    sso, 
    appConsole, 
    widgetInterface, 
    widgetGateway, 
    kbs, 
    applicationType,
    locale,
    portal
  });
}