import express from 'express';
import path from 'path';
import api from '../api';
import config from '../../config/environment';

export default (app) => {
	let router = express.Router();
	
	router.use('/static/', express.static(path.join(config.root, 'server', 'static')));
  router.use('/static/', express.static(path.join(config.root, 'public', 'static')));
  router.use(express.static(path.join(config.root, 'build')));

  if(process.env.NODE_ENV === 'production'){
    router.use('/', express.static(path.join(config.root, 'public')));
  }

  api(app)

  router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
  });

  api(app)

	router.get('*', (req,res) =>{
    res.sendFile(path.join(config.root, 'public/index.html'));
  });
	// add middleware here

	return router;
}
