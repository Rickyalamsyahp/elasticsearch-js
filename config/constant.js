export const themes = {
  PAJAKKU     : 'pajakku',
  SOBATPAJAK  : 'sobatpajak'
}

export const applicationTypes = {
  INTERNAL    : 'internal',
  PRODUCT     : 'product',
  GENERAL     : 'general'
}

export const efiling = {
  PATH : "/djp/get_ntte/{filename}/{ntpa}",
}

export const Bkwp = {
  PATH : "/bkwp/konfirmasi/npwp",
}