const {
  override,
  fixBabelImports,
  addLessLoader,
  addWebpackAlias
} = require("customize-cra");
const path = require('path');


module.exports = override(
  addWebpackAlias({
    ['src']: path.resolve(__dirname, './src'),
    ['assets']: path.resolve(__dirname, './src/assets'),
    ['config']: path.resolve(__dirname, './src/config'),
    ['libs']: path.resolve(__dirname, './src/libs'),
    ['modules']: path.resolve(__dirname, './src/modules'),
    ['services']: path.resolve(__dirname, './src/services')
  })
);
