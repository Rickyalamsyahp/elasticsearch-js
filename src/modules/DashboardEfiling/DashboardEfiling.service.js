import { http } from "../../libs/react-mpk/services";

export const getSummaryEfilling = () => http.get(`/dashboard/efelling`);

export const getChartEfilling = () => http.get(`/dashboard/chart/efelling`);
