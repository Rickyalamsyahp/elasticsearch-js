import React from "react";
import { DashboardWrapper } from "../../libs/react-mpk/wrapper";
import * as service from "./DashboardEfiling.service";

const DashboardEfiling = () => {
  return (
    <DashboardWrapper
      baseId="mod-dashboard-sample-id"
      title="Dashboard Efiling"
      className="mpk-full full-height"
      definitions={[
        new DashboardWrapper.definition(
          null,
          [
            new DashboardWrapper.summaries(service.getSummaryEfilling, (data) =>
              data.map(
                (d) =>
                  new DashboardWrapper.summaryData(
                    d.label,
                    d.value,
                    d.unit,
                    d.trend,
                    d.trendUnit
                  )
              )
            ),
          ],
          [
            new DashboardWrapper.chart(
              "Chart Efeling",
              service.getChartEfilling,
              (data) =>
                data.map(
                  (d) =>
                    new DashboardWrapper.chartData(d.category, d.key, d.value)
                ),
              { type: "bar" },
              {}
            ),
          ]
        ),
      ]}
    />
  );
};

export default DashboardEfiling;
