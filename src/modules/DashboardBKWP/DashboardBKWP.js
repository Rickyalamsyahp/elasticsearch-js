import React from "react";
import { DashboardWrapper } from "../../libs/react-mpk/wrapper";
import * as service from "./DashboardBKWP.service";

const DashboardBKWP = () => {
  return (
    <DashboardWrapper
      baseId="mod-dashboard-sample-id"
      title="Dashboard BKWP API"
      className="mpk-full full-height"
      definitions={[
        new DashboardWrapper.definition(
          null,
          [
            new DashboardWrapper.summaries(service.getSummaryBKWP, (data) =>
              data.map(
                (d) =>
                  new DashboardWrapper.summaryData(
                    d.label,
                    d.value,
                    d.unit,
                    d.trend,
                    d.trendUnit
                  )
              )
            ),
          ],
          [
            new DashboardWrapper.chart(
              "Chart BWK API",
              service.getChartBKWP,
              (data) =>
                data.map(
                  (d) =>
                    new DashboardWrapper.chartData(d.category, d.key, d.value)
                ),
              { type: "bar" },
              {}
            ),
          ]
        ),
      ]}
    />
  );
};

export default DashboardBKWP;
