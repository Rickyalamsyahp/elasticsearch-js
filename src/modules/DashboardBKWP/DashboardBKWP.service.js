import { http } from "../../libs/react-mpk/services";

export const getSummaryBKWP = () => http.get(`/dashboard/bkwp`);

export const getChartBKWP = () => http.get(`/dashboard/chart/bkwp`);
