import DashboardEfiling from "./DashboardEfiling";
import DashboardBKWP from "./DashboardBKWP";
import DetailsSample from "./DetailsSample";
import FormSample from "./FormSample";
import TableSample from "./TableSample";

export {
  DashboardEfiling,
  DashboardBKWP,
  DetailsSample,
  FormSample,
  TableSample,
};
