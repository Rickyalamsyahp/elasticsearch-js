import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import { PageWrapper } from "../../libs/react-mpk/wrapper";
import { sso } from "../../libs/react-mpk/hocs";
import {
  DashboardEfiling,
  DashboardBKWP,
  TableSample,
} from "../../modules";

const Company = ({ history }) => {
  return (
    <PageWrapper>
      <Router history={history}>
        <Switch>
          <Route
            path="/product/company/:companyId/dashboard"
            render={(props) => <DashboardEfiling {...props} />}
          />
          <Route
            path="/product/company/:companyId/dashboard"
            render={(props) => <DashboardBKWP {...props} />}
          />
          <Route
            path="/product/company/:companyId/table"
            render={(props) => <TableSample {...props} />}
          />
        </Switch>
      </Router>
    </PageWrapper>
  );
};

export default sso({
  basePath: "/product/company/:companyId",
  url: {
    me: "/api/sso/company/:companyId/me",
  },
})(Company);
